package com.example.rotatingtabtest;

import android.content.Context;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.FrameLayout;

/**
 * Custom FrameLayouts for fractional values.
 * @author killjoy
 *
 */
public class FractionTranslateFrameLayout extends FrameLayout {
    private static final String TAG = FractionTranslateFrameLayout.class.getName();
    private Context context;
    
    public FractionTranslateFrameLayout(Context context) {
    	super(context);
    	this.context = context;
    }

    public FractionTranslateFrameLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public float getXFraction()
    {
    	WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        return (width == 0) ? 0 : getX() / (float) width;
    }

    public void setXFraction(float xFraction) {
    	WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
    	Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        setX((width > 0) ? (xFraction * width) : 0);
    }
}