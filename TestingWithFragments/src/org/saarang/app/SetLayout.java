package org.saarang.app;


import android.content.Context;
import android.graphics.Color;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

public class SetLayout {
	
	private static final int INSIDE_EVENT = 10;
	private static final int INSIDE_SPOTLIGHT = 12;
	private static final int INSIDE_INFORMALS = 13;
	private static final int SPOTLIGHT_BACKGROUND = R.drawable.back_inside_spotlight;
	private static final int SPOTLIGHT_COLOR = R.color.epurple;
	private static final int INFORMALS_BACKGROUND = R.drawable.backinformals;
	private static final int INFORMALS_COLOR = R.color.saarangRed;
	private static final int EVENTS_BACKGROUND = R.drawable.backevents;
	private static final int EVENTS_COLOR = R.color.supercyan;
	private static final String TAB_1 ="Tab_1";
	private static final String TAB_2 ="Tab_2";
	private static final String TAB_3 ="Tab_3";
	private Context context;
	private TabHost tabHost;
	private TextView tv;
	private int frag;
	private RelativeLayout rl;
	private ScrollView tab1, tab2;
	private RelativeLayout tab3;
	
	public SetLayout(Context c, RelativeLayout rel, TabHost tb, TextView t, ScrollView sv1, ScrollView sv2, RelativeLayout rl3, int fr){
		this.context = c;
		this.rl = rel;
		this.tabHost = tb;
		this.tv = t;
		this.frag = fr;
		this.tab1 = sv1;
		this.tab2 = sv2;
		this.tab3 = rl3;
	}
	public void setupLayout(){
		setupTabs();
		setBackground();
		setTextBackground();
	}
	
	public void setBackground(){
		if(frag==INSIDE_SPOTLIGHT)
			rl.setBackgroundResource(SPOTLIGHT_BACKGROUND);
		else if(frag==INSIDE_INFORMALS)
			rl.setBackgroundResource(INFORMALS_BACKGROUND);
		else if(frag==INSIDE_EVENT)
			rl.setBackgroundResource(EVENTS_BACKGROUND);
	}
	
	public void setTextBackground(){
		tv.setBackgroundColor(context.getResources().getColor(getFragColorId(frag)));
	}
	
	public void setupTabs(){
		tabHost.setup();
		
		TabSpec spec1=tabHost.newTabSpec(TAB_1);
        spec1.setContent(R.id.tab1);
        spec1.setIndicator("About");

        TabSpec spec2=tabHost.newTabSpec(TAB_2);
        spec2.setIndicator("Register");
        spec2.setContent(R.id.tab2);

        TabSpec spec3=tabHost.newTabSpec(TAB_3);
        spec3.setIndicator("Contacts");
        spec3.setContent(R.id.tab3);
		
		tabHost.addTab(spec1);
		tabHost.addTab(spec2);
		tabHost.addTab(spec3);
		
		tabHost.getTabWidget().getChildAt(0).setBackgroundColor(context.getResources().getColor(getFragColorId(frag)));
		TextView tv =(TextView)tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
		tv.setTextColor(Color.WHITE);
		
		tab1.setBackgroundColor(context.getResources().getColor(getFragColorId(frag)));
		tab2.setBackgroundColor(context.getResources().getColor(getFragColorId(frag)));
		tab3.setBackgroundColor(context.getResources().getColor(getFragColorId(frag)));
	}
	
	public void tabChanged(String str){
		int i = getTabId(str);
		int id = getFragColorId(frag);
		if(i!=-1){
			for(int j = 0; j < 3; j++){
				if(i==j){
					tabHost.getTabWidget().getChildAt(j).setBackgroundColor(context.getResources().getColor(id));
					TextView tv =(TextView)tabHost.getTabWidget().getChildAt(j).findViewById(android.R.id.title);
					tv.setTextColor(Color.WHITE);
				}
				else{
					tabHost.getTabWidget().getChildAt(j).setBackgroundColor(Color.WHITE);
					TextView tv =(TextView)tabHost.getTabWidget().getChildAt(j).findViewById(android.R.id.title);
					tv.setTextColor(Color.BLACK);
				}
			}
		}
	}
	
	public int getTabId(String tabPress){
		if(tabPress.compareTo(TAB_1)==0) return 0;
		else if(tabPress.compareTo(TAB_2)==0) return 1;
		else if(tabPress.compareTo(TAB_3)==0) return 2;
		else return -1;
	}
	
	public int getFragColorId(int frag){
		if(frag==INSIDE_SPOTLIGHT) return SPOTLIGHT_COLOR;
		else if(frag==INSIDE_INFORMALS) return INFORMALS_COLOR;
		else if(frag == INSIDE_EVENT) return EVENTS_COLOR;
		else return 0;
	}
}
