package org.saarang.app;



import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;
import android.widget.ImageView;

public class EventsFragment extends Fragment implements OnTouchListener,
		OnClickListener {

	private static final int id = 11;

	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	boolean wait2 = false;
	private ImageButton dance, panache, media, writing, music, quizzing, zone,
			lectures, fine, sudoku, classical2, speaking, classical, tamil,
			word, thespian;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.eventsfrag, container, false);
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		db = new DiskButtons(getActivity().getApplicationContext());
		choose = (ImageView) getActivity().findViewById(R.id.disk);

		dance = (ImageButton) getActivity().findViewById(R.id.events_dance);
		panache = (ImageButton) getActivity().findViewById(R.id.events_panache);
		media = (ImageButton) getActivity().findViewById(R.id.events_media);
		writing = (ImageButton) getActivity().findViewById(R.id.events_writing);
		music = (ImageButton) getActivity().findViewById(R.id.events_music);
		quizzing = (ImageButton) getActivity().findViewById(
				R.id.events_quizzing);
		zone = (ImageButton) getActivity().findViewById(R.id.events_zone);
		lectures = (ImageButton) getActivity().findViewById(R.id.events_lec);
		fine = (ImageButton) getActivity().findViewById(R.id.events_fine);
		sudoku = (ImageButton) getActivity().findViewById(R.id.events_sudoku);
		classical2 = (ImageButton) getActivity().findViewById(
				R.id.events_classical2);
		speaking = (ImageButton) getActivity().findViewById(
				R.id.events_speaking);
		classical = (ImageButton) getActivity().findViewById(
				R.id.events_classical);
		tamil = (ImageButton) getActivity().findViewById(R.id.events_tamil);
		word = (ImageButton) getActivity().findViewById(R.id.events_word);
		thespian = (ImageButton) getActivity().findViewById(
				R.id.events_thespian);

		dance.setOnClickListener(this);
		panache.setOnClickListener(this);
		media.setOnClickListener(this);
		writing.setOnClickListener(this);
		music.setOnClickListener(this);
		quizzing.setOnClickListener(this);
		zone.setOnClickListener(this);
		lectures.setOnClickListener(this);
		fine.setOnClickListener(this);
		sudoku.setOnClickListener(this);
		classical2.setOnClickListener(this);
		speaking.setOnClickListener(this);
		classical.setOnClickListener(this);
		tamil.setOnClickListener(this);
		word.setOnClickListener(this);
		thespian.setOnClickListener(this);

		choose.setOnTouchListener(this);
		comm = (Communicator) getActivity();

		final Animation anim = AnimationUtils.loadAnimation(getActivity(),
				R.animator.anim_button);
		dance.startAnimation(anim);
		panache.startAnimation(anim);
		media.startAnimation(anim);
		writing.startAnimation(anim);
		music.startAnimation(anim);
		quizzing.startAnimation(anim);
		zone.startAnimation(anim);
		lectures.startAnimation(anim);
		fine.startAnimation(anim);
		sudoku.startAnimation(anim);
		classical2.startAnimation(anim);
		speaking.startAnimation(anim);
		classical.startAnimation(anim);
		tamil.startAnimation(anim);
		word.startAnimation(anim);
		thespian.startAnimation(anim);
	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		if (nextAnim != 0) {
			Animator anim = AnimatorInflater.loadAnimator(getActivity(),
					nextAnim);
			anim.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationStart(Animator animation) {
					wait = true;
				}

				@Override
				public void onAnimationEnd(Animator animation) {
					wait = false;
				}
			});
			return anim;
		} else
			return null;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		int evX = (int) event.getX();
		int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.events_dance)
			comm.respondEvent(id, "Dance Events", 0);
		else if (v.getId() == R.id.events_panache)
			comm.respondInsideEvent(id-1, 32);
		else if (v.getId() == R.id.events_media)
			comm.respondEvent(id, "Media Events", 7);
		else if (v.getId() == R.id.events_writing)
			comm.respondEvent(id, "Writing Events", 9);
		else if (v.getId() == R.id.events_music)
			comm.respondEvent(id, "Music Events", 2);
		else if (v.getId() == R.id.events_quizzing)
			comm.respondEvent(id, "Quizzing Events", 3);
		else if (v.getId() == R.id.events_zone)
			comm.respondInsideEvent(id-1, 3);
		else if (v.getId() == R.id.events_lec)
			//TODO Figure out whether to use lecdems single or list>single and ...
			comm.respondEvent(id, "Lectures & Demo's", 5);
		else if (v.getId() == R.id.events_fine)
			comm.respondEvent(id, "Fine Arts", 6);
		else if (v.getId() == R.id.events_sudoku)
			comm.respondInsideEvent(id-1, 48);
		else if (v.getId() == R.id.events_classical2)
			comm.respondEvent(id, "Classical Arts", 10);
		else if (v.getId() == R.id.events_speaking)
			comm.respondEvent(id, "Speaking Events", 1);
		else if (v.getId() == R.id.events_classical)
			comm.respondEvent(id, "Classical Arts", 10);
		else if (v.getId() == R.id.events_tamil)
			comm.respondInsideEvent(id-1, 49);
		else if (v.getId() == R.id.events_word)
			comm.respondEvent(id, "Wordgames", 8);
		else if (v.getId() == R.id.events_thespian)
			comm.respondEvent(id, "Thespian Events", 4);
	}
}
