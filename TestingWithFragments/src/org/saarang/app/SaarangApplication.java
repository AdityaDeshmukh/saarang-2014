package org.saarang.app;


import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.PushService;

public class SaarangApplication extends Application{
	@Override
	public void onCreate() {
		super.onCreate();

		// Add your initialization code here
		 Parse.initialize(this, "c39soVyRy8MRGfSQO2hLF8GQkJpVi6VqDJQB9WBS", "OUzton9WRjgk6iYt2nwv15xYsukSTXCn6W7VICNS");

		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();

		// If you would like all objects to be private by default, remove this
		// line.
		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);
		
		//Test
//		ParseObject testObject = new ParseObject("TestObject");
//		testObject.put("foo", "bar");
//		testObject.saveInBackground();
		
		//Set callback activity on receiving a push. TODO
		PushService.subscribe(getApplicationContext(), "all", ReceivePush.class);
		PushService.setDefaultPushCallback(this, ReceivePush.class);
//		PushService.setDefaultPushCallback(this, MainActivity.class);
//		PushService.subscribe(getApplicationContext(), "all", MainActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}
}
