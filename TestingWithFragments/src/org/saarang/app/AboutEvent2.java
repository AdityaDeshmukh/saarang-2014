//package org.saarang.app;
//
//import java.util.Locale;
//
//import android.support.v4.app.Fragment;
//import android.content.ActivityNotFoundException;
//import android.content.Intent;
//import android.net.Uri;
//import android.os.Bundle;
//import android.support.v4.app.FragmentManager;
//import android.support.v4.app.FragmentPagerAdapter;
//import android.support.v4.view.ViewPager;
//import android.util.Log;
//import android.view.LayoutInflater;
//import android.view.MotionEvent;
//import android.view.View;
//import android.view.View.OnClickListener;
//import android.view.View.OnTouchListener;
//import android.view.ViewGroup;
//import android.widget.AdapterView;
//import android.widget.AdapterView.OnItemClickListener;
//import android.widget.ArrayAdapter;
//import android.widget.Button;
//import android.widget.ImageView;
//import android.widget.ListView;
//import android.widget.RelativeLayout;
//import android.widget.ScrollView;
//import android.widget.TabHost;
//import android.widget.TabHost.OnTabChangeListener;
//import android.widget.TextView;
//import android.widget.Toast;
//
//public class AboutEvent extends Fragment implements OnClickListener,
//		OnTouchListener, OnItemClickListener {
//
//	private static final String GET_FRAGMENT = "frag";
//	private static final String GET_TITLE = "title";
//	private static final String GET_ABOUT = "about";
//	private static final String GET_PRIZES = "prizes";
//	private static final String GET_REG = "reg";
//	private static final String GET_NAMES = "names";
//	private static final String GET_MOBILES = "mobiles";
//	private static final String GET_IDOFEVENT = "idofevent";
//
//	private TextView title, about, prizes, register;
//	Communicator comm;
//	ImageView choose;
//	DiskButtons db;
//	boolean wait = false;
//	boolean wait2 = false;
//	Button toEventRegButton;
//	Button toCreateTeamButton;
//	private ListView contacts;
//	private View v;
//	private long idOfEvent;
//	int frag;
//	String mobileList[];
//	ArrayAdapter<String> adapter;
//
//	@Override
//	public View onCreateView(LayoutInflater inflater, ViewGroup container,
//			Bundle savedInstanceState) {
//		v = inflater.inflate(R.layout.about_event2, container, false);
//		title = (TextView) v.findViewById(R.id.inside_info_tv);
//		about = (TextView) v.findViewById(R.id.textOfTab1);
//		register = (TextView) v.findViewById(R.id.textOfTab2);
//		toEventRegButton = (Button) v.findViewById(R.id.toEventRegButton);
//		toCreateTeamButton = (Button) v.findViewById(R.id.toCreateTeamButton);
//		contacts = (ListView) v.findViewById(R.id.textOfTab3);
//
//		toEventRegButton.setOnClickListener(this);
//		toCreateTeamButton.setOnClickListener(this);
//
//		// Create the adapter that will return a fragment for each of the three
//		// primary sections of the app.
//		mSectionsPagerAdapter = new SectionsPagerAdapter(
//				getSupportFragmentManager());
//
//		// Set up the ViewPager with the sections adapter.
//		mViewPager = (ViewPager) getActivity().findViewById(R.id.pager);
//		mViewPager.setAdapter(mSectionsPagerAdapter);
//
//		return v;
//	}
//
//	@Override
//	public void onActivityCreated(Bundle savedInstanceState) {
//		super.onActivityCreated(savedInstanceState);
//		setRetainInstance(true);
//		comm = (Communicator) getActivity();
//		db = new DiskButtons(getActivity().getApplicationContext());
//		choose = (ImageView) getActivity().findViewById(R.id.disk);
//		choose.setOnTouchListener(this);
//
//		Bundle data = getArguments();
//		frag = data.getInt(GET_FRAGMENT);
//		idOfEvent = data.getLong(GET_IDOFEVENT);
//		mobileList = data.getStringArray(GET_MOBILES);
//		title.setText(data.getString(GET_TITLE));
//		String[] list = data.getStringArray(GET_NAMES);
//		if (list != null) {
//			adapter = new ArrayAdapter<String>(getActivity(),
//					android.R.layout.simple_list_item_1, list);
//			contacts.setAdapter(adapter);
//		}
//		contacts.setOnItemClickListener(this);
//		about.setText(data.getString(GET_ABOUT));
//		prizes.setText(data.getString(GET_PRIZES));
//		register.setText(data.getString(GET_REG));
//	}
//
//	@Override
//	public boolean onTouch(View v, MotionEvent event) {
//		// Log.v("pivotTabs debug", "in onTouch");
//		int action = event.getAction();
//		int evX = (int) event.getX();
//		int evY = (int) event.getY();
//		switch (action) {
//		case MotionEvent.ACTION_DOWN:
//		case MotionEvent.ACTION_MOVE:
//			db.changeImage(choose,
//					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
//			break;
//		case MotionEvent.ACTION_UP:
//			choose.setImageDrawable(getActivity().getResources().getDrawable(
//					R.drawable.disk_unpressed));
//			if (!wait)
//				db.checkButton(comm,
//						(ImageView) getActivity().findViewById(R.id.mask), evX,
//						evY);
//			break;
//		}
//		return true;
//	}
//
//	@Override
//	public void onClick(View v) {
//		MainActivity mainActivity = (MainActivity) getActivity();
//		Log.v("tag", "" + v.getId() + " " + R.id.toCreateTeamButton);
//		if (v.getId() == R.id.toEventRegButton) {
//			if (mainActivity.loggedIn == false) {
//				Toast.makeText(getActivity().getApplicationContext(),
//						"Please login before registering", Toast.LENGTH_SHORT)
//						.show();
//				return;
//			}
//			comm.respondToButton(4, idOfEvent);
//		}
//		if (v.getId() == R.id.toCreateTeamButton) {
//			if (mainActivity.loggedIn == false) {
//				Toast.makeText(getActivity().getApplicationContext(),
//						"Please login before registering", Toast.LENGTH_SHORT)
//						.show();
//				return;
//			}
//			comm.respondToButton(5, idOfEvent);
//		}
//	}
//
//	@Override
//	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
//		try {
//			Intent callIntent = new Intent(Intent.ACTION_DIAL);
//			callIntent.setData(Uri.parse("tel:+" + mobileList[arg2]));
//			startActivity(callIntent);
//		} catch (ActivityNotFoundException e) {
//			e.printStackTrace();
//		}
//	}
//
//	/**
//	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
//	 * one of the sections/tabs/pages.
//	 */
//	private class SectionsPagerAdapter extends FragmentPagerAdapter {
//
//		private static final int FRAGMENT_COUNT = 4;
//
//		public SectionsPagerAdapter(FragmentManager fm) {
//			super(fm);
//		}
//
//		@Override
//		public Fragment getItem(int position) {
//			// getItem is called to instantiate the fragment for the given page.
//			
//
//			switch (position) {
//			//TODO implement all managerial functions
//			case 0:
//				Fragment fragment0 = new ();
//				return fragment0;
//				
//				// Return a DummySectionFragment (defined as a static inner class
//				// below) with the page number as its lone argument.
//			default:
//				//Dummy default implementation
//				Fragment fragment = new DummySectionFragment();
//				Bundle args = new Bundle();
//				args.putInt(DummySectionFragment.ARG_SECTION_NUMBER,
//						position + 1);
//				fragment.setArguments(args);
//				return fragment;
//			}
//		}
//
//		@Override
//		public int getCount() {
//			// Show 3 total pages.
//			return FRAGMENT_COUNT;
//		}
//
//		@Override
//		public CharSequence getPageTitle(int position) {
//			Locale l = Locale.getDefault();
//			switch (position) {
//			case 0:
//				return getString(R.string.title_section1).toUpperCase(l);
//			case 1:
//				return getString(R.string.title_section2).toUpperCase(l);
//			case 2:
//				return getString(R.string.title_section3).toUpperCase(l);
//			}
//			return null;
//		}
//	}
//}