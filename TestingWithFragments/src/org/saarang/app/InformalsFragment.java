package org.saarang.app;



import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

public class InformalsFragment extends Fragment implements OnTouchListener, OnClickListener{
	
	private static final int id = 13;
	
	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	boolean wait2 = false;
	ImageButton street_info,carnival_info,treasure_info,cluedo_info,ganimatoonic_info,potpurri_info,scavenger_info,ship_info,workshop_info;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.informalsfrag, container, false);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		
		db = new DiskButtons(getActivity().getApplicationContext());
		
		choose = (ImageView) getActivity().findViewById(R.id.disk);
		choose.setOnTouchListener(this);
		
		street_info = (ImageButton) getActivity().findViewById(R.id.info_street);
		carnival_info = (ImageButton) getActivity().findViewById(R.id.info_carnival);
		treasure_info = (ImageButton) getActivity().findViewById(R.id.info_treasure);
		cluedo_info = (ImageButton) getActivity().findViewById(R.id.info_cluedo);
		ganimatoonic_info = (ImageButton) getActivity().findViewById(R.id.info_ganimatoonic);
		potpurri_info = (ImageButton) getActivity().findViewById(R.id.info_potpurri);
		scavenger_info = (ImageButton) getActivity().findViewById(R.id.info_scavenger);
		ship_info = (ImageButton) getActivity().findViewById(R.id.info_ship);
		workshop_info = (ImageButton) getActivity().findViewById(R.id.info_workshop);
		
		street_info.setOnClickListener(this);
		carnival_info.setOnClickListener(this);
		treasure_info.setOnClickListener(this);
		cluedo_info.setOnClickListener(this);
		ganimatoonic_info.setOnClickListener(this);
		potpurri_info.setOnClickListener(this);
		scavenger_info.setOnClickListener(this);
		ship_info.setOnClickListener(this);
		workshop_info.setOnClickListener(this);

		comm=(Communicator) getActivity();
		
		final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.animator.anim_button);
		street_info.startAnimation(anim);
		carnival_info.startAnimation(anim);
		treasure_info.startAnimation(anim);
		cluedo_info.startAnimation(anim);
		ganimatoonic_info.startAnimation(anim);
		potpurri_info.startAnimation(anim);
		scavenger_info.startAnimation(anim);
		ship_info.startAnimation(anim);
		workshop_info.startAnimation(anim);
	}
	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		if(nextAnim!=0){
			Animator anim = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
	        anim.addListener(new AnimatorListenerAdapter() {
	        	@Override
	            public void onAnimationStart(Animator animation) {
	        		wait=true;
	            }
	            @Override
	            public void onAnimationEnd(Animator animation) {
	            	wait=false;
	            }
	        });
	        return anim;
		}
		else
			return null;
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		final int action = event.getAction();
		final int evX = (int) event.getX();
		final int evY = (int) event.getY();
		switch (action) {
			case MotionEvent.ACTION_DOWN :
			case MotionEvent.ACTION_MOVE:
				db.changeImage(choose,(ImageView)getActivity().findViewById(R.id.mask), evX, evY);
				break;
			case MotionEvent.ACTION_UP :
				choose.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.disk_unpressed));
				if(!wait)
					db.checkButton(comm, (ImageView)getActivity().findViewById(R.id.mask), evX, evY);
				break;
		}
		return true;
	}
	@Override
	public void onClick(View v) {
			if(v.getId()==R.id.info_street) comm.respondInsideEvent(id, 47);
			else if(v.getId()==R.id.info_carnival) comm.respondInsideEvent(id, 6);
			else if(v.getId()==R.id.info_treasure) comm.respondInsideEvent(id, 51);
			else if(v.getId()==R.id.info_cluedo) comm.respondInsideEvent(id, 9);
			else if(v.getId()==R.id.info_ganimatoonic) comm.respondInsideEvent(id, 22);
			else if(v.getId()==R.id.info_potpurri) comm.respondInsideEvent(id, 34);
			else if(v.getId()==R.id.info_scavenger) comm.respondInsideEvent(id, 37);
			else if(v.getId()==R.id.info_ship) comm.respondInsideEvent(id, 40);
			else if(v.getId()==R.id.info_workshop) comm.respondInsideEvent(id, 54);
	}
}
