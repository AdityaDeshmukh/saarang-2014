package org.saarang.app;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Vector;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.util.Pair;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static String TAG = "DatabaseHelper";
	private static String DB_PATH = "";
	public static String DB_NAME ="saarang_new";

	public static final String EVENT_TABLE_NAME = "events";
	//public static final String KEY_EVENT_ROWID = "rowid";
	public static final String KEY_EVENT_ID = "rowid";
	public static final String KEY_EVENT_NAME = "Name";
	public static final String KEY_GROUP_ID = "DeptId";
	public static final String KEY_EVENT_ABOUT = "About";
	public static final String KEY_EVENT_IS_TEAM = "is_team";
	public static final String KEY_EVENT_REGISTRATION_OPEN = "registration_open";
	//public static final String KEY_EVENT_PRIZES = "Prizes";
	public static final String KEY_EVENT_TIME_PLACE = "timeandplace";

	public static final String COORD_TABLE_NAME = "coordTables";
	public static final String KEY_COORD_ROWID = "rowid";
	public static final String KEY_COORD_NAME = "Name";
	public static final String KEY_COORD_ID = "_id";
	public static final String KEY_COORD_EVENT_ID = "Event_id";
	public static final String KEY_COORD_MOBILE = "phoneNumber";


	private SQLiteDatabase mDataBase;
	private final Context mContext;

	public DatabaseHelper(Context context) {
	    super(context, DB_NAME, null, 1);
	    if(android.os.Build.VERSION.SDK_INT >= 17){
	       DB_PATH = context.getApplicationInfo().dataDir + "/databases/";
	    }
	    else {
	       DB_PATH = "/data/data/" + context.getPackageName() + "/databases/";
	    }
	    this.mContext = context;
	}

	public void createDataBase() throws IOException {
	    //If database not exists copy it from the assets
	    boolean mDataBaseExist = checkDataBase();
	    if(!mDataBaseExist) {
	        this.getReadableDatabase();
	        this.close();
	        try {
	            //Copy the database from assets
	            copyDataBase();
	            Log.e(TAG, "createDatabase database created");
	        }
	        catch (IOException mIOException) {
	            throw new Error("ErrorCopyingDataBase");
	        }
	    }
	}
    //Check that the database exists here: /data/data/your package/databases/Da Name
    private boolean checkDataBase() {
        File dbFile = new File(DB_PATH + DB_NAME);
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDataBase() throws IOException {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0) {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
        mOutput.close();
        mInput.close();

    }

    //Open the database, so we can query it
    public boolean openDataBase() throws SQLException {
        String mPath = DB_PATH + DB_NAME;
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        return mDataBase != null;
    }

    @Override
    public synchronized void close() {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

	@Override
	public void onCreate(SQLiteDatabase db) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		db.execSQL("DROP TABLE IF EXISTS events");
		onCreate(db);
	}
	public Cursor fetchDescription(long eventId){
		String[] columns = new String[]{/*KEY_EVENT_ROWID,*/ KEY_EVENT_ID, KEY_EVENT_NAME, KEY_GROUP_ID, KEY_EVENT_ABOUT, KEY_EVENT_IS_TEAM,  KEY_EVENT_REGISTRATION_OPEN, KEY_EVENT_TIME_PLACE};
		Cursor c = mDataBase.query(EVENT_TABLE_NAME, columns, KEY_EVENT_ID + "=" + eventId, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	public Cursor fetchCoordDetails(long eventId){
		String[] columns = new String[]{KEY_COORD_ROWID, KEY_COORD_NAME, KEY_COORD_ID, KEY_COORD_EVENT_ID, KEY_COORD_MOBILE};
		Cursor c = mDataBase.query(COORD_TABLE_NAME, columns, KEY_COORD_EVENT_ID + "=" + eventId, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
	public Vector<Pair<String, String>> getEventList() {
		String[] columns = new String[]{KEY_EVENT_ID, KEY_EVENT_NAME};
		if(mDataBase == null) {
			Log.i("DB LOG", "mDataBase not created.");
		}
		Cursor c = //mDataBase.rawQuery("select " + KEY_EVENT_NAME + " , " + KEY_EVENT_ID + " from "  + EVENT_TABLE_NAME, null);
				mDataBase.query(EVENT_TABLE_NAME, columns, null, null, null, null, null);
		Vector<Pair<String, String>> listEvents = new Vector<Pair<String, String>>();
		if( c != null) {
			c.moveToFirst();
			while(c.isAfterLast() == false) {
				listEvents.add(new Pair<String, String>(c.getString(0), c.getString(1)));
			}
		}
		return listEvents;
	}
	public Cursor getList(){
		String[] columns = new String[]{KEY_EVENT_ID, KEY_EVENT_NAME};
		Cursor c = mDataBase.query(EVENT_TABLE_NAME, columns, null, null, null, null, null);
		if (c != null) {
			c.moveToFirst();
		}
		return c;
	}
}
