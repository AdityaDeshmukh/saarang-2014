package org.saarang.app;

import android.app.Activity;
import android.os.Bundle;
import android.widget.TextView;

public class ReachSaarang extends Activity{
	TextView tv;
	private String s = "Outstation participants can reach the IIT Madras campus via bus, auto or call-taxi which is available just outside the station.\n\nIITM campus:\n\nIs located on the Sardar Patel Road\nIs midway between the Raj Bhavan and Madyakailash.\nIs 13 kilometers from Chennai Central Railway Station\nLandmarks -\n\nAdjacent to Guindy Snake Park, Adyar Cancer Institute\nOpposite to Central Leather Research Institute and Anna University\nA. From Chennai Central\n\nHire an auto/taxi to IIT Madras..\nYou can catch a bus just outside the Chennai Central. 18B and 19S are the direct buses to IITM (Get down at Gandhi Mandapam /IITM). From the gate there are IITM buses to the hostels, every 20 minutes.\nB. From Egmore Station:\n\nHire an auto/taxi to IIT Madras.\nCatch 23C which heads directly to IITM. From the main gate there are IITM buses to the hostels, every 20 minutes\nC. From Airport:\n\nHire an auto/taxi to IIT Madras. Inside the campus, get down at the Hospitality Control Room.\nTake bus number PP21 directly to institute or 21G to Gandhi Mandapam which is at a stone throw's distance from out gate. From the main gate there are IITM buses to the hostels, every 20 minutes.\nD. From CMBT:\n\nHire an auto/taxi to IIT Madras (inside)\nCatch Bus number 23M or 5E which come directly to institute. From the main gate there are IITM buses to the hostels, every 20 minutes.";
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
			setContentView(R.layout.reach);
			tv = (TextView) findViewById(R.id.reachText);
			tv.setText(s);
	}
}