package org.saarang.app;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class HomeFragment extends Fragment implements OnTouchListener,
		OnClickListener {
	ImageView OrangeView, GreenView, BlueView, WhiteView, SaarangText;
	Communicator comm;
	ImageView choose;
	DiskButtons db;
	Button toLoginButton, toRegisterButton, menu_button;
	boolean wait = false;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.homefrag, container, false);
		toLoginButton = (Button) view.getRootView().findViewById(R.id.toLogin);
		toRegisterButton = (Button) view.getRootView().findViewById(
				R.id.toRegister);
		OrangeView = (ImageView) view.getRootView().findViewById(R.id.orange);
		BlueView = (ImageView) view.getRootView().findViewById(R.id.blue);
		GreenView = (ImageView) view.getRootView().findViewById(R.id.green);
		WhiteView = (ImageView) view.getRootView().findViewById(R.id.white);
		SaarangText = (ImageView) view.getRootView().findViewById(
				R.id.saarang_text);
		menu_button = (Button) view.getRootView()
				.findViewById(R.id.menu_button);
		// Display Register and Login on completion of animation
		// Log.v("Debug toLoginReg", "in onCreatedActivity");
		// toLoginButton.setAlpha(0);
		// toRegisterButton.setAlpha(0);

		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		db = new DiskButtons(getActivity().getApplicationContext());

		choose = (ImageView) getActivity().findViewById(R.id.disk);

		choose.setOnTouchListener(this);
		comm = (Communicator) getActivity();

		// emailRegistered = (TextView) getActivity().findViewById(
		// R.id.emailRegistered);
		// saarangId = (TextView) getActivity().findViewById(R.id.saarangId);
		menu_button.setOnClickListener(this);
		// toRegisterButton.setOnClickListener(this);
		toLoginButton.setOnClickListener(this);
		Log.i("Home Layout debug 1", OrangeView.getHeight() + " , "
				+ OrangeView.getWidth());
		Log.i("Home Layout debug 1",
				GreenView.getHeight() + " , " + GreenView.getWidth());
		Log.i("Home Layout debug 1",
				WhiteView.getHeight() + " , " + WhiteView.getWidth());
		
		InitAnim();
		KeyAnim();
		CopyReadAssets("schedule.pdf");
		CopyReadAssets("hotel.pdf");

		/*
		 * Display display =
		 * getActivity().getWindowManager().getDefaultDisplay(); DisplayMetrics
		 * outMetrics = new DisplayMetrics (); display.getMetrics(outMetrics);
		 * 
		 * float density =
		 * getActivity().getResources().getDisplayMetrics().density; float
		 * dpHeight = outMetrics.heightPixels / density; float dpWidth =
		 * outMetrics.widthPixels / density; menu_button.setWidth((int)
		 * (dpWidth/2)); toLoginButton.setWidth((int) (dpWidth/2));
		 */
		Log.i("Home Layout debug 12", OrangeView.getHeight() + " , "
				+ OrangeView.getWidth());
		Log.i("Home Layout debug 22",
				GreenView.getHeight() + " , " + GreenView.getWidth());
		Log.i("Home Layout debug 32",
				WhiteView.getHeight() + " , " + WhiteView.getWidth());
	}

	@Override
	public void onResume() {
		super.onResume();
		MainActivity mainActivity = (MainActivity) getActivity();
		if (mainActivity.loggedIn == true) {
			toLoginButton.setText("Logout");
			// saarangId.setVisibility(View.VISIBLE);
			// emailRegistered.setText(mainActivity.emailRegistered);
			// emailRegistered.setVisibility(View.VISIBLE);
		}
	}

	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		if (nextAnim != 0) {
			Animator anim = AnimatorInflater.loadAnimator(getActivity(),
					nextAnim);
			anim.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationStart(Animator animation) {
					wait = true;
				}

				@Override
				public void onAnimationEnd(Animator animation) {
					wait = false;
				}
			});
			return anim;
		} else
			return null;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		final int action = event.getAction();
		final int evX = (int) event.getX();
		final int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}

	public void InitAnim() {

		ObjectAnimator RotatAnim = ObjectAnimator.ofFloat(OrangeView,
				"rotation", 150);
		RotatAnim.setDuration(1);
		RotatAnim.start();

		RotatAnim = ObjectAnimator.ofFloat(GreenView, "rotation", 200);
		RotatAnim.setDuration(1);
		RotatAnim.start();

		RotatAnim = ObjectAnimator.ofFloat(BlueView, "rotation", 300);
		RotatAnim.setDuration(1);
		RotatAnim.start();

		ObjectAnimator FadeAnim = ObjectAnimator.ofFloat(WhiteView, "alpha", 0);
		FadeAnim.setDuration(1);
		FadeAnim.start();
	}

	public void KeyAnim() {

		ObjectAnimator RotatAnim1 = ObjectAnimator.ofFloat(OrangeView,
				"rotation", 0);
		RotatAnim1.setDuration(1500);
		RotatAnim1.setInterpolator(new DecelerateInterpolator());

		ObjectAnimator RotatAnim2 = ObjectAnimator.ofFloat(GreenView,
				"rotation", 360);
		RotatAnim2.setDuration(1500);
		RotatAnim2.setInterpolator(new DecelerateInterpolator());

		ObjectAnimator RotatAnim3 = ObjectAnimator.ofFloat(BlueView,
				"rotation", 0);
		RotatAnim3.setDuration(1500);
		RotatAnim3.setInterpolator(new DecelerateInterpolator());

		ObjectAnimator FadeAnim = ObjectAnimator.ofFloat(WhiteView, "alpha",
				100);
		FadeAnim.setDuration(1000);

		// Animation for login/register buttons
		// ObjectAnimator FadeAnim2 = ObjectAnimator.ofFloat(toLoginButton,
		// "alpha", 100);
		// FadeAnim2.setDuration(1000);
		// ObjectAnimator FadeAnim3 = ObjectAnimator.ofFloat(toRegisterButton,
		// "alpha", 100);
		// FadeAnim3.setDuration(1000);

		AnimatorSet LockAnim = new AnimatorSet();

		LockAnim.play(RotatAnim1);
		LockAnim.play(RotatAnim2).after(500);
		LockAnim.play(RotatAnim3).after(1200);
		LockAnim.play(FadeAnim).after(1500);
		// LockAnim.play(FadeAnim2);
		// LockAnim.play(FadeAnim3);

		ObjectAnimator Scalex1 = ObjectAnimator.ofFloat(OrangeView, "scaleX",
				(float) 1.5);
		ObjectAnimator Scalex2 = ObjectAnimator.ofFloat(GreenView, "scaleX",
				(float) 1.5);
		ObjectAnimator Scalex3 = ObjectAnimator.ofFloat(BlueView, "scaleX",
				(float) 1.5);
		ObjectAnimator Scalex4 = ObjectAnimator.ofFloat(WhiteView, "scaleX",
				(float) 1.5);
		ObjectAnimator Scaley1 = ObjectAnimator.ofFloat(OrangeView, "scaleY",
				(float) 1.5);
		ObjectAnimator Scaley2 = ObjectAnimator.ofFloat(GreenView, "scaleY",
				(float) 1.5);
		ObjectAnimator Scaley3 = ObjectAnimator.ofFloat(BlueView, "scaleY",
				(float) 1.5);
		ObjectAnimator Scaley4 = ObjectAnimator.ofFloat(WhiteView, "scaleY",
				(float) 1.5);

		AnimatorSet ScaleAnim = new AnimatorSet();
		ScaleAnim.play(Scalex1).with(Scalex2).with(Scalex3).with(Scalex4)
				.with(Scaley4).with(Scaley1).with(Scaley2).with(Scaley3);
		ScaleAnim.setInterpolator(new AccelerateDecelerateInterpolator());
		ScaleAnim.setDuration(1);

		ScaleAnim.start();
		LockAnim.setStartDelay(100);
		LockAnim.start();
	}

	private void CopyReadAssets(String filename) {
		AssetManager assetManager = getActivity().getAssets();
		InputStream in = null;
		OutputStream out = null;
		File file = new File(getActivity().getFilesDir(), filename);
		try {
			in = assetManager.open(filename);
			out = getActivity().openFileOutput(file.getName(),
					Context.MODE_WORLD_READABLE);
			copyFile(in, out);
			in.close();
			in = null;
			out.flush();
			out.close();
			out = null;
		} catch (Exception e) {
			Log.e("tag", e.getMessage());
		}
	}

	private void copyFile(InputStream in, OutputStream out) throws IOException {
		byte[] buffer = new byte[1024];
		int read;
		while ((read = in.read(buffer)) != -1) {
			out.write(buffer, 0, read);
		}
	}

	@Override
	public void onClick(View v) {
		// TODO Verify
		if (v.getId() == R.id.toLogin) {
			MainActivity mainActivity = (MainActivity) getActivity();
			if (mainActivity.loggedIn == true) {
				comm.respondToButton(3, "-1", false);
			}
			comm.respondToButton(2, "-1", false);
		} else if (v.getId() == R.id.menu_button) {
			AlertDialog.Builder menu = new AlertDialog.Builder(getActivity());
			menu.setIcon(R.drawable.app_icon_hd);
			MainActivity mainActivity = (MainActivity) getActivity();
			menu.setTitle(" "
					+ ((mainActivity.saarangId != null) ? mainActivity.saarangId
							: "Menu"));
			final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
					getActivity(), android.R.layout.simple_list_item_1);
			arrayAdapter.add("Reach Saarang");
			arrayAdapter.add("Event Schedule");
			arrayAdapter.add("See Map");
			arrayAdapter.add("View Previous Notifications");
			arrayAdapter.add("Important Contacts");
			arrayAdapter.add("Alternate Accommodation");
			arrayAdapter.add("Credits");
			menu.setNegativeButton("Back",
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
						}
					});

			menu.setAdapter(arrayAdapter,
					new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialog, int which) {
							switch (which) {
							case 0:
								Intent reach = new Intent(getActivity(),
										ReachSaarang.class);
								startActivity(reach);
								break;
							case 1:
								Intent schedule = new Intent(Intent.ACTION_VIEW);
								schedule.setDataAndType(
										Uri.parse("file://"
												+ getActivity().getFilesDir()
												+ "/schedule.pdf"),
										"application/pdf");
								startActivity(schedule);
								break;
							case 2:
								Intent map = new Intent(getActivity(),
										Map.class);
								startActivity(map);
								break;
							case 3:
								Intent notif = new Intent(getActivity(),
										ReceivePush.class);
								startActivity(notif);
								break;
							case 4:
								Intent contacts = new Intent(getActivity(),
										ImportantContacts.class);
								startActivity(contacts);
								break;
							case 5:
								Intent hotel = new Intent(Intent.ACTION_VIEW);
								hotel.setDataAndType(
										Uri.parse("file://"
												+ getActivity().getFilesDir()
												+ "/hotel.pdf"),
										"application/pdf");
								startActivity(hotel);
								break;
							case 6:
								Intent credits = new Intent(getActivity(),
										Credits.class);
								startActivity(credits);
								break;
							}
						}
					});
			menu.show();
			// WebView wb;
			/*
			 * Intent contacts = new Intent(getActivity(), OtherMenu.class);
			 * startActivity(contacts);
			 */
		}
	}
}
