package org.saarang.app;

import android.app.ExpandableListActivity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ImportantContacts extends ExpandableListActivity {
    private static final String NAME = "NAME";
    private static final String NUMBER = "NUMBER";
    
    private ExpandableListAdapter mAdapter;
    private String[] group = {"Hospitality","Hospital","Security","Sponsorship","Taxi Services"};
    private String[] child1 = {"Hotline 1","Hotline 2","Hotline 3","Hospitality Core : Aditya"};
    private String[] number1 = {"04422578126","04422578127"," 04422578128","+919444357599"};
    private String[] child2 = {"Institute Hosptial"};
    private String[] number2 = {"04422578888"};
    private String[] child3 = {"Security Section"};
    private String[] number3 = {"04422579999"};
    private String[] child4 = {"Sponsorship Core : Achutan"};
    private String[] number4 = {"+919884314049"};
    private String[] child5 = {"Fast Track","Friends Track"};
    private String[] number5 = {"04460006000","04430063006"};
    private String[][] list_name = {child1, child2, child3, child4, child5};
    private String[][] list_number = {number1,number2,number3,number4,number5};
    private int[] lenghts = {child1.length,child2.length,child3.length,child4.length,child5.length};
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();
        for (int i = 0; i < group.length; i++) {
            Map<String, String> curGroupMap = new HashMap<String, String>();
            groupData.add(curGroupMap);
            curGroupMap.put(NAME, group[i]);
            //curGroupMap.put(IS_EVEN, (i % 2 == 0) ? "This group is even" : "This group is odd");
            
            List<Map<String, String>> children = new ArrayList<Map<String, String>>();
            for (int j = 0; j < lenghts[i]; j++) {
                Map<String, String> curChildMap = new HashMap<String, String>();
                children.add(curChildMap);
                curChildMap.put(NAME, list_name[i][j]);
                curChildMap.put(NUMBER, list_number[i][j]);
            }
            childData.add(children);
        }
        
        // Set up our adapter
        mAdapter = new SimpleExpandableListAdapter(
                this,
                groupData,
                android.R.layout.simple_expandable_list_item_1,
                new String[] { NAME, NUMBER },
                new int[] { android.R.id.text1, android.R.id.text2 },
                childData,
                android.R.layout.simple_expandable_list_item_2,
                new String[] { NAME, NUMBER },
                new int[] { android.R.id.text1, android.R.id.text2 }
                );
        setListAdapter(mAdapter);
    }

	@Override
	public boolean onChildClick(ExpandableListView parent, View v,
			int groupPosition, int childPosition, long id) {
		// TODO Auto-generated method stub
		try {
			Intent callIntent = new Intent(Intent.ACTION_DIAL);
			callIntent.setData(Uri.parse("tel:" + list_number[groupPosition][childPosition]));
			startActivity(callIntent);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
		return super.onChildClick(parent, v, groupPosition, childPosition, id);
	}


	@Override
	public void onBackPressed() {
		super.onBackPressed();
		//overridePendingTransition(R.animator.bottom_in, R.animator.stationary);
	}
}
