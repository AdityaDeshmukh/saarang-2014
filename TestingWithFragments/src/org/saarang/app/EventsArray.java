package org.saarang.app;

public class EventsArray {

	String[] dance = {"Choreo Night","Solo freestyle","Dance Wokshops","$treet$","Just Duet"};
    int[]  danceId = {7, 41, 13, 46,26};
	String[] speaking = {"JAM","Young Reporter","Elocution","Shipwrek"};
    int[]  speakingId = {25, 14, 19, 40};
	String[] music = {"Power Chords","Tarang","Acoustyx","Acapella","Sonata","Vox","Unplugged","Dj'uke Box","Decibels","Alankar"};
    int[] musicID = {35, 50, 2, 1, 42, 53, 52, 17, 15, 4};
	String[] quizzing ={"Spent Quiz","India Quiz","Lone Wolf","Nature & Environment Quiz","Buzzer Quiz", "Ganimatoonic Quiz"};
    int[] quizzingId = {44, 23, 28, 88, 5, 22};
	String[] thespian ={"Mad-ads","Street Play", "Monoacting","Saarang Dramatics"};
    int[] thespianId = {29, 45, 31, 18};
	String[] lectures = {"Kiran Bedi","Mahesh Dattani","Leander Paes","Nawazuddin Siddique","Leela Samson","Manu Joseph","Vineeth 'Beep' Kumar","Chitra Banerjee Divakaruni"};
    //TODO 41 is NOT lecdems Aditya...
	int[] lecturesId = {};
	String[] finearts = {"Logo Affiche","T-Shirt Designing Workshop","Arty-Stix","Strokes","Expression","Klay Krafting","Montage","Tatoo Painting Workshop","Painting","Abstract Art Workshop"};
    int[] fineArtsIDs = {76, 77, 20, 71, 72, 73, 75, 78, 59, 40};
	String[] media = {"Ad-Film Making Contest","Photography Contest","SFM Contest","Online Photography Contest"};
    int[] mediaIds = {82, 80, 81, 82};
	String[] wordgames = {"Crossie","Scrabble","Spell Bee","WTGW"};
    int[] wordGamesID = {11, 38, 43, 55};
	String[] writing = {"Flash Fiction","Online Creative Writing"};
    //TODO fix negative thingy, or remove it.
	int[] writingID = {21, 10};
	String[] classical = {"Classical Dance Group","Classical Music Vocal","Classical Dance Solo","Classical Music Instrument"};
    int[] classicalId = {84, 86, 85, 87};
	String[][] list= {dance,speaking,music,quizzing,thespian,lectures,finearts,media,wordgames,writing,classical};
	int[][] listnumber = {danceId,speakingId,musicID,quizzingId,thespianId,lecturesId,fineArtsIDs,mediaIds,wordGamesID,writingID,classicalId};
	String groupNameList[] = {"Dance Events","Speaking Events","Music Events","Quizzing Events", "Thespian Events", "Lectures & Demos", "Fine Arts", "Media Events", "Word Games", "Writing Events", "Classical Arts"};
	
	public String[] getString(int pos){
		return list[pos];
	}
	
	
	public int[] getInts(int pos) {
		return listnumber[pos];
	}
}
