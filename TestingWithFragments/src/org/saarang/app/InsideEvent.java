package org.saarang.app;



import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class InsideEvent extends Fragment implements OnItemClickListener,OnTouchListener{
	
	private String GET_LIST_NUMBER = "listnumber";
	//private String GET_TITLE = "title";
	private int id = 10;
	TextView tv;
	ListView lv;
	Communicator comm;
	int listNumber;
	ArrayAdapter<String> adapter;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.inside_event, container, false);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		comm=(Communicator) getActivity();		
		db = new DiskButtons(getActivity().getApplicationContext());
		choose = (ImageView) getActivity().findViewById(R.id.disk);
		choose.setOnTouchListener(this);
		
		Bundle data = getArguments();
		tv = (TextView) getActivity().findViewById(R.id.inside_event_tv);
		lv = (ListView) getActivity().findViewById(R.id.eventlist);
		//tv.setText(data.getString(GET_TITLE));
		EventsArray ea = new EventsArray();
		String[] list = data.getStringArray("list");
		listNumber = data.getInt(GET_LIST_NUMBER);
		tv.setText(ea.groupNameList[listNumber]);
		if(list!=null){
			adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_list_item_1, list);
			lv.setAdapter(adapter);
			lv.setOnItemClickListener(this);
		}
	}
	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		EventsArray ae = new EventsArray();
		int[] eventsList = ae.getInts(listNumber);
		if(listNumber==5) {
			//comm.respondInsideEvent(id, eventsList[0]);
			return;
		}
		comm.respondInsideEvent(id, eventsList[arg2]);
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		int evX = (int) event.getX();
		int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}
}
