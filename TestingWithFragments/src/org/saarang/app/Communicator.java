package org.saarang.app;

public interface Communicator {
	public void respond(int frag);
	public void respondEvent(int frag, String text, int eventPos);
	public void respondInsideEvent(int frag, long id);
	public void respondProshow(int frag, int id);
	public void respondToButton(int toWhere, String idOfEvent, boolean isTeam);
	public void respondSearch(int eventid);
}
