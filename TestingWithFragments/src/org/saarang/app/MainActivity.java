package org.saarang.app;

import java.io.IOException;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Window;
import android.widget.ImageView;

public class MainActivity extends Activity implements Communicator {

	private static final int HOME = 1;
	private static final int SPOTLIGHT = 2;
	private static final int EVENTS = 3;
	private static final int PROSHOWS = 4;
	private static final int INFORMALS = 5;
	private static final int SEARCH = 6;
	private static final int ABOUT_EVENT = 10;
	private static final int INSIDE_EVENT = 11;
	private static final int INSIDE_SPOTLIGHT = 12;
	private static final int INSIDE_INFORMALS = 13;
	private static final int INSIDE_PROSHOWS = 14;
	private static final int REGISTER = 21;
	private static final int LOGIN = 22;
	private static final int LOGOUT = 23;
	private static final int EVENTREGISTER = 24;
	private static final int TEAMREGISTER = 25;
	
	private static final String PUT_FRAGMENT = "frag";
	private static final String PUT_TITLE = "title";
	private static final String PUT_ABOUT = "about";
	// private static final String PUT_PRIZES = "prizes";
	private static final String PUT_NAMES = "names";
	private static final String PUT_MOBILES = "mobiles";
	private static final String PUT_LIST = "list";
	private static final String PUT_LIST_NUMBER = "listnumber";
	private static final String PUT_IDOFEVENT = "idofevent";
	private static final String PUT_REG = "reg";
	private static final String PUT_ISTEAM = "isteam";
	private static final String PUT_PROSHOW = "proshow";

	private static final int ID_COLUMN = 0;
	private static final int NAME_COLUMN = 1;
	private static final int GROUP_ID_COLUMN = 2;
	private static final int ABOUT_COLUMN = 3;
	// private static final int PRIZES_COLUMN = 5;
	private static final int IS_TEAM_COLUMN = 4;
	private static final int IS_REGISTRATION_OPEN_COLUMN = 5;
	private static final int TIME_PLACE_COLUMN = 6;

	private static final int COORD_NAME_COLUMN = 1;
	private static final int COORD_ID_COLUMN = 3;
	// private static final int COORD_ID_COLUMN = 2;
	private static final int COORD_MOBILE_COLUMN = 4;
	private boolean onPauseVisited = false;

	public boolean loggedIn = false;

	public String saarangId;
	public int proshow;
	public String emailRegistered;

	FragmentManager manager = getFragmentManager();
	EventsArray ea = new EventsArray();
	ImageView choose;
	int prevId = 1;
	String setText = null;
	// The nth list in the hard-coded lists of group events.
	private int setListNumber;
	private String idOfEvent;
	private boolean isTeam;
	String setList[];
	String coordList[][];
	DatabaseHelper myDbHelper;
	Cursor event_cursor, coord_cursor;
	SearchLayout sl = new SearchLayout();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		this.requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		//Log.e("<---Test--->",manager.findFragmentById(R.id.main).toString());
		myDbHelper = new DatabaseHelper(this);
		try {
			myDbHelper.createDataBase();
		} catch (IOException ioe) {
			throw new Error("Unable to create database");
		}
		try {
			myDbHelper.openDataBase();
		} catch (SQLException sqle) {
			throw sqle;
		}
		setContentView(R.layout.activity_main);
		if(manager.findFragmentById(R.id.main)==null){
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.add(R.id.main, getFrag(HOME), "Home");
			transaction.commit();
		}
		
		//Log.e("<---Test--->", "onCreate");
		
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		menu.add("Credits");
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		/*
		 * Toast toast = Toast.makeText(getApplicationContext(),
		 * "Credits \n Saarang MobOps 2014 \n \t Aditya Deshmukh \n \t Suhas Gundimeda \n \t Abhinav Garlapati"
		 * , Toast.LENGTH_LONG); toast.setGravity(Gravity.CENTER_HORIZONTAL, 0,
		 * 0); toast.show();
		 */
		Intent credits = new Intent(MainActivity.this, Credits.class);
		startActivity(credits);
		return false;
	}

	@Override
	protected void onPause() {
		super.onPause();
		//Log.e("<---Test--->", "onPause");
		/*FragmentTransaction transaction = manager.beginTransaction();
		transaction.remove(getFrag(prevId));
		transaction.commit();
		onPauseVisited = true;*/
	}

	@Override
	protected void onResume() {
		super.onResume();
		SharedPreferences sharedPrefs = getSharedPreferences(
				"org.saarang.app", Context.MODE_PRIVATE);
		loggedIn = sharedPrefs.getBoolean("loggedin", false);

		//Log.e("<---Test--->", "onResume");
		
		/*
		 * if(onPauseVisited){ FragmentTransaction transaction =
		 * manager.beginTransaction(); transaction.add(R.id.main,
		 * getFrag(prevId)); transaction.commit(); onPauseVisited=false; }
		 */
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		myDbHelper.close();
		//Log.e("<---Test--->", "onDestroy");
	}

	@Override
	public void respond(int frag) {
		// Log.v("debug PivotTabs", ""+findFragment(prevId));
		if (frag == SEARCH) {
			/*
			 * Intent i = new Intent(this, SearchLayout.class);
			 * startActivity(i); overridePendingTransition(0,
			 * R.animator.slide_out);
			 */
			FragmentTransaction transaction = manager.beginTransaction();
			transaction.replace(R.id.main2, sl);
			transaction.addToBackStack(null);
			// transaction.remove(fragment);
			transaction.commit();
			// } else if (frag != prevId && findFragment(prevId)) {
		} else if (frag != prevId) {
			// Log.v("pivot tabs test", "in respond for frag "+ frag);
			// if(id!=prevId){
			if (frag > prevId)
				setTransaction(1, frag);
			else if (frag < prevId)
				setTransaction(2, frag);
			else
				setTransaction(5, frag);
			prevId = frag;
		}
	}

	@Override
	public void respondEvent(int frag, String text, int eventPos) {
		if (eventPos != -1) {
			setList = ea.getString(eventPos);
			setListNumber = eventPos;
		} else
			setList = null;

		setText = text;
		setTransaction(6, frag);
		prevId = frag;
	}

	@Override
	public void respondInsideEvent(int frag, long id) {
		event_cursor = myDbHelper.fetchDescription(id);
		coord_cursor = myDbHelper.fetchCoordDetails(id);
		this.idOfEvent = String.valueOf(id);
		int num = coord_cursor.getCount();
		if (num != 0) {
			int i = 0;
			coordList = new String[2][num];
			for (coord_cursor.moveToFirst(); !coord_cursor.isAfterLast(); coord_cursor
					.moveToNext(), i++) {
				coordList[0][i] = coord_cursor.getString(COORD_NAME_COLUMN);
				coordList[1][i] = coord_cursor.getString(COORD_MOBILE_COLUMN);
			}
		}
		setTransaction(3, frag);
		prevId = frag;
	}

	@Override
	public void respondProshow(int frag, int id) {
		this.proshow = id;
		if(id==6) {
			Intent intent = new Intent(this, WorldFestActivity.class);
			startActivity(intent);
			return;
		}
		setTransaction(4, frag);
		prevId = frag;
	}

	/**
	 * Calls various Fragment transactions for the login flow.
	 */
	@Override
	public void respondToButton(int toWhere, String idOfEvent, boolean isTeam) {
		this.idOfEvent = idOfEvent;
		this.isTeam = isTeam;
		setTransaction(7, toWhere + 20);
		prevId = toWhere + 20;
	}

	public void setTransaction(int flag, int id) {
		FragmentTransaction transaction = manager.beginTransaction();
		if (flag == 1) {
			manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			transaction.setCustomAnimations(R.animator.rotate_right_in,
					R.animator.rotate_left_out);
			transaction.replace(R.id.main, getFrag(id), tagFragment(id));
			// transaction.addToBackStack(String.valueOf(id));
			transaction.commit();
		} else if (flag == 2) {
			manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
			transaction.setCustomAnimations(R.animator.rotate_left_in,
					R.animator.rotate_right_out);
			transaction.replace(R.id.main, getFrag(id), tagFragment(id));
			// transaction.addToBackStack(String.valueOf(id));
			transaction.commit();
		} else if (flag == 3) {
			transaction.setCustomAnimations(R.animator.bottom_in,
					R.animator.top_out, R.animator.top_in,
					R.animator.bottom_out);
			Fragment frag = getFrag(id);
			transaction.replace(R.id.main, frag);
			Bundle data = new Bundle();
			data.putInt(PUT_FRAGMENT, id);
			// Log.v("debug idof evnt", ""+idOfEvent);
			data.putString(PUT_IDOFEVENT, idOfEvent);
			data.putString(PUT_TITLE, event_cursor.getString(NAME_COLUMN));
			// Log.v("debug title" , event_cursor.getString(NAME_COLUMN));
			data.putString(PUT_ABOUT, event_cursor.getString(ABOUT_COLUMN));
			// data.putString(PUT_PRIZES,
			// event_cursor.getString(PRIZES_COLUMN));
			// Log.v("debug is_team",(event_cursor.getString(IS_TEAM_COLUMN).equals("1"))
			// + "");
			data.putBoolean(PUT_ISTEAM, (event_cursor.getString(IS_TEAM_COLUMN)
					.equals("1")) ? true : false);
			// data.putBoolean(PUT_REG,
			// (event_cursor.getString(IS_REGISTRATION_OPEN_COLUMN)=="1")?true:false);
			data.putStringArray(PUT_NAMES, coordList[0]);
			data.putStringArray(PUT_MOBILES, coordList[1]);
			// data.putString(PUT_CONTACTS,coord_cursor.getString(CONTACTS_COLUMN));
			transaction.addToBackStack(null);
			frag.setArguments(data);
			transaction.commit();

		}
		// Proshows fragments.
		else if (flag == 4) {
			transaction.setCustomAnimations(R.animator.bottom_in,
					R.animator.top_out, R.animator.top_in,
					R.animator.bottom_out);
			Fragment frag = getFrag(id);
			Bundle data = new Bundle();
			data.putInt(PUT_PROSHOW, proshow);
			frag.setArguments(data);
			transaction.replace(R.id.main, frag);
			transaction.addToBackStack(null);
			transaction.commit();
		} else if (flag == 5) {
			Fragment frag = getFrag(id);
			transaction.replace(R.id.main, frag);
			transaction.addToBackStack(null);
			transaction.commit();
		}
		// For loading lists of evens in group events
		else if (flag == 6) {
			transaction.setCustomAnimations(R.animator.bottom_in,
					R.animator.top_out, R.animator.top_in,
					R.animator.bottom_out);
			Fragment frag = getFrag(id);
			Bundle data = new Bundle();
			data.putInt(PUT_FRAGMENT, id);
			data.putStringArray(PUT_LIST, setList);
			data.putInt(PUT_LIST_NUMBER, setListNumber);
			frag.setArguments(data);
			transaction.replace(R.id.main, frag);
			transaction.addToBackStack(null);
			transaction.commit();
		}
		// For moving between the various backend fragments, 'id' is 'toWhere'
		// in caller.
		else if (flag == 7) {
			Fragment frag = null;
			Bundle data = new Bundle();
			switch (id) {
			// Register
			case REGISTER:
				frag = new RegisterFragment();
				break;
			// Login
			case LOGIN:
				frag = new LoginFragment();
				break;
			// Logout
			case LOGOUT:
				frag = new LogoutFragment();
				break;
			// Event Register
			case EVENTREGISTER:
				frag = new EventRegisterFragment();
				// Log.v("debug idofevent", ""+idOfEvent);
				data.putString(PUT_IDOFEVENT, "" + idOfEvent);
				data.putBoolean(PUT_ISTEAM, isTeam);
				frag.setArguments(data);
				break;
			// Create team
			case TEAMREGISTER:
				frag = new CreateTeamFragment();
				// Log.v("debug Trans set 7", "" + idOfEvent);
				data.putString(PUT_IDOFEVENT, "" + idOfEvent);
				frag.setArguments(data);
				break;
			default:
				return;
			}
			transaction.replace(R.id.main, frag);
			transaction.addToBackStack(null);
			transaction.commit();
		}
	}

	public Fragment getFrag(int id) {
		HomeFragment hf = new HomeFragment();
		SpotlightFragment sf = new SpotlightFragment();
		EventsFragment ef = new EventsFragment();
		ProshowsFragment pf = new ProshowsFragment();
		InformalsFragment inf = new InformalsFragment();
		InsideEvent ie = new InsideEvent();
		AboutEvent ae = new AboutEvent();
		InsideProshows ip = new InsideProshows();
		// Set up backend fragments
		switch (id) {
		case HOME:
			return hf;
		case SPOTLIGHT:
			return sf;
		case EVENTS:
			return ef;
		case PROSHOWS:
			return pf;
		case INFORMALS:
			return inf;
		case INSIDE_EVENT:
			return ie;
		case ABOUT_EVENT:
			return ae;
		case INSIDE_SPOTLIGHT:
			return ae;
		case INSIDE_INFORMALS:
			return ae;
		case INSIDE_PROSHOWS:
			return ip;
		default:
			return null;
		}
	}

	public boolean findFragment(int id) {
		switch (id) {
		case 0:
			return true;
		case HOME:
			if (manager.findFragmentByTag("Home") != null)
				return true;
			else
				return false;
		case SPOTLIGHT:
			if (manager.findFragmentByTag("Spotlight") != null)
				return true;
			else
				return false;
		case EVENTS:
			if (manager.findFragmentByTag("Events") != null)
				return true;
			else
				return false;
		case PROSHOWS:
			if (manager.findFragmentByTag("Proshows") != null)
				return true;
			else
				return false;
		case INFORMALS:
			if (manager.findFragmentByTag("Informals") != null)
				return true;
			else
				return false;
		default:
			return false;
		}
	}

	public String tagFragment(int id) {
		switch (id) {
		case HOME:
			return "Home";
		case SPOTLIGHT:
			return "Spotlight";
		case EVENTS:
			return "Events";
		case PROSHOWS:
			return "Proshows";
		case INFORMALS:
			return "Informals";
		default:
			return null;
		}
	}

	@Override
	public void respondSearch(int id) {
		FragmentTransaction transaction = manager.beginTransaction();
		manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
		transaction.remove(sl);
		transaction.commit();
		event_cursor = myDbHelper.fetchDescription(id);
		coord_cursor = myDbHelper.fetchCoordDetails(id);
		this.idOfEvent = String.valueOf(id);
		int num = coord_cursor.getCount();
		if (num != 0) {
			int i = 0;
			coordList = new String[2][num];
			for (coord_cursor.moveToFirst(); !coord_cursor.isAfterLast(); coord_cursor
					.moveToNext(), i++) {
				coordList[0][i] = coord_cursor.getString(COORD_NAME_COLUMN);
				coordList[1][i] = coord_cursor.getString(COORD_MOBILE_COLUMN);
			}
		}
		setTransaction(3, ABOUT_EVENT);
	}
}
