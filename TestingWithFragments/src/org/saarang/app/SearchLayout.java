package org.saarang.app;

import java.util.ArrayList;
import java.util.HashMap;

import android.os.Bundle;
import android.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class SearchLayout extends Fragment {

	EditText edittext;
	ListView listview;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	Communicator comm;
	
	private String[] text = {"Acapella","Acoustyx","Adrenaline Zone","Alankar","Buzzer Quiz","Carnival","Choreo Night","Classical Arts","Cluedo","Online Saarang Writing Awards","Crossie","Daily Events","Dance Workshop","Young Reporter","Decibels","Design Fest","DJukebox","Dramatics","Eloc","Arty-Stix","Flash Fiction","Ganimatoonic","India Quiz","Informals","JAM","Just Duet","Lectures and Demonstrations","Lone Wolf","Mad-Ads","Mobile Informals","Monoacting","Panache","Photography","PotPourri","Powerchords","Roadshows","Scavenger Hunt","Scrabble","SFM","Shipwreck","Solo freestyle","Sonata","Spell Bee","SpEnt Quiz","Street Play","$treet$","Street Sports","Sudoku","Tamil Stand Up ","Tarang","Treasure Hunt","LM Unplugged","Vox","Workshops","WTGW","Painting","Versatile Writing","Strokes","Expressions","Klay Krafting","Montage","Logo Affiche","T-shirt Designing Workshop","Abstarct Art Workshop","On-Spot Photography Contest","Saarang Film Festival","AD-APT","Big Shot","Classical Dance Group","Classical Dance Solo","Classical Music Vocal","Classical Music Instrumental","Nature and Environment Quiz","Femina Miss India & Saarang 2014 present Walk to Fame","Sahara Force India PitStop Challenge","Online Photography Contest","Education For All Run"};
    private int[] id_list = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51,52,53,54,55,59,62,71,72,73,75,76,77,78,80,81,82,83,84,85,86,87,88,91,92,93,94};
    HashMap<String, Integer> eventIdHash = new HashMap<String, Integer>();
	int textlength = 0;

	ArrayList<String> text_sort = new ArrayList<String>();
	ArrayList<Integer> id_sort = new ArrayList<Integer>();
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.search, container, false);
	}

	public void onActivityCreated(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		db = new DiskButtons(getActivity().getApplicationContext());

		comm = (Communicator) getActivity();
		for(int i = 0; i < text.length; i++) {
			eventIdHash.put(text[i], id_list[i]);
			text_sort.add(text[i]);
			id_sort.add(id_list[i]);
		}
		edittext = (EditText) getActivity().findViewById(R.id.EditText01);
		listview = (ListView) getActivity().findViewById(R.id.ListView01);
		if(listview==null){
			Log.e("---ERR", "");
		}
		listview.setAdapter(new CustomAdapter(text, id_list));
		listview.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				//Log.i("TEST", "Event Name = " + text_sort.get(arg2) + " Event Id = " + id_sort.get(arg2));
				comm.respondSearch(eventIdHash.get(text_sort.get(arg2)));
				// TODO Auto-generated method stub
				
			}
		});

		edittext.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {

			}

			public void beforeTextChanged(CharSequence s, int start, int count,
					int after) {

			}

			public void onTextChanged(CharSequence s, int start, int before,
					int count) {

				textlength = edittext.getText().length();
				text_sort.clear();
				id_sort.clear();

				/*for (int i = 0; i < text.length; i++) {
					if (textlength <= text[i].length()) {
						if (edittext
								.getText()
								.toString()
								.equalsIgnoreCase(
										(String) text[i].subSequence(0,
												textlength))) {
							text_sort.add(text[i]);
						}
					}
				}
*/
				for(int i = 0; i < text.length; i++) {
					if(text[i].toLowerCase().contains(edittext.getText().toString().toLowerCase())){
						text_sort.add(text[i]);
						id_sort.add(id_list[i]);
					}
				}
				listview.setAdapter(new CustomAdapter(text_sort));

			}
		});
	}

	class CustomAdapter extends BaseAdapter {

		String[] data_text;
		int[] data_id;

		CustomAdapter() {

		}

		CustomAdapter(String[] text, int[] id) {
			data_text = text;
			data_id   = id;
		}

		CustomAdapter(ArrayList<String> text) {

			data_text = new String[text.size()];

			for (int i = 0; i < text.size(); i++) {
				data_text[i] = text.get(i);
			}

		}

		public int getCount() {
			return data_text.length;
		}

		public String getItem(int position) {
			return null;
		}

		public long getItemId(int position) {
			return position;
		}

		public View getView(int position, View convertView, ViewGroup parent) {

			LayoutInflater inflater = getActivity().getLayoutInflater();
			View row;

			row = inflater.inflate(R.layout.listview, parent, false);

			TextView textview = (TextView) row.findViewById(R.id.TextView01);

			textview.setText(data_text[position]);

			return (row);

		}
	}	
}