package org.saarang.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;



import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class RegisterFragment extends Fragment implements OnClickListener, OnTouchListener {

	private ProgressBar progressBar;
	private Button registerButton;
	Communicator comm;
	DiskButtons db;
	ImageView choose;
	boolean wait = false;

	// private String csrfToken;

	// private final String CSRF_COOKIE_DOMAIN = "saarang.org";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_register, container,
				false);

		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

		// Check whether Network is availabile
		if (isNetworkAvailable() == false) {
			Toast.makeText(getActivity(),
					"Please try again when network is available",
					Toast.LENGTH_LONG).show();
			return view;
		}

		registerButton = (Button) view.findViewById(R.id.action_register_button);
		registerButton.setOnClickListener(this);
		// Send GET request to retrieve CSRF token
		// HttpGet httpGet = new
		// HttpGet("http://erp.saarang.org/main/register/csrf/");
		// new SendGet().execute(httpGet);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		db = new DiskButtons(getActivity().getApplicationContext());
		choose = (ImageView) getActivity().findViewById(R.id.disk);

		choose.setOnTouchListener(this);
		comm = (Communicator) getActivity();

	}
	
	@Override
	public void onClick(View v) {
		if (v == null) {
			Toast.makeText(getActivity(),
					"View is null for some reason! Contact the developer",
					Toast.LENGTH_SHORT).show();
			return;
		}

		// Retrieving data from form fields
		String deviceId = Secure.getString(getActivity()
				.getApplicationContext().getContentResolver(),
				Secure.ANDROID_ID);
		String mobile = ((EditText) v.getRootView().findViewById(R.id.mobile))
				.getText().toString();
		String email = ((EditText) v.getRootView().findViewById(R.id.email))
				.getText().toString();
		String college = ((EditText) v.getRootView().findViewById(R.id.college))
				.getText().toString();
		String name = ((EditText) v.getRootView().findViewById(R.id.name))
				.getText().toString();
		String password = ((EditText) v.getRootView().findViewById(
				R.id.password)).getText().toString();
		String repassword = ((EditText) v.getRootView().findViewById(
				R.id.repassword)).getText().toString();
		String gender = ((RadioButton) v.getRootView().findViewById(
				((RadioGroup) v.getRootView().findViewById(R.id.radioGroup1))
						.getCheckedRadioButtonId())).getText().toString();
		if (email.length() == 0 || password.length() == 0 || repassword.length() == 0 || college.length() == 0 || name.length() == 0 || mobile.length() == 0) {
			Toast.makeText(getActivity(), "Please enter email, password, 10-digit mobile number, college and gender",
					Toast.LENGTH_SHORT).show();
			return;
		}

		// //Getting base 64 encoded email
		// byte[] data = null;
		// try {
		// data = email.getBytes("UTF-8");
		// } catch (UnsupportedEncodingException e1) {
		// // Auto-generated catch block
		// e1.printStackTrace();
		// }
		//
		// String base64email = Base64.encodeToString(data, Base64.DEFAULT);
		//
		// Sending to an AsyncTask to POST data
		HttpPost httpPost = new HttpPost(
				"http://www.erp.saarang.org/mobile/register/");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		// nameValuePairs.add(new
		// BasicNameValuePair("csrfmiddlewaretoken",csrfToken));
		nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		nameValuePairs.add(new BasicNameValuePair("gender", gender));
		nameValuePairs.add(new BasicNameValuePair("mobile", mobile));
		nameValuePairs.add(new BasicNameValuePair("college", college));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		nameValuePairs.add(new BasicNameValuePair("repassword", repassword));
		nameValuePairs.add(new BasicNameValuePair("key", deviceId));
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		if(!isNetworkAvailable()){
			Toast.makeText(getActivity(),
					"Please try again when network is available",
					Toast.LENGTH_LONG).show();
			return;
		}
		progressBar.setVisibility(View.VISIBLE);
		new SendPost().execute(httpPost);
	}

	public void renderResponse(String responseContent) {
		// Display dialogues for each response
		// char success =
		// responseContent.charAt(responseContent.indexOf("<body>")+7);
		char success = responseContent.charAt(0);
		Log.v("responseChar", String.valueOf(success));
		String messageToShow;
		switch (success) {
		case 'S':
			messageToShow = "Successfully registered! Please check your email and activate your account.";
			break;
		case 'W':
			messageToShow = "Passwords don't match, please try again";
			break;
		case 'N':
			messageToShow = "Email not registered, enter other details to register, or try again";
			break;
		case 'P':
			messageToShow = "Please provide a 10-digit valid phone number";
			break;
		case 'I':
			messageToShow = "Please provide a valid e-mail address, it's required to activate your account";
			break;
		case 'A':
			messageToShow = "Account not activated! Please check your email(spam/junk too) and activate your account";
			break;
		case 'R':
			messageToShow = "This e-mail has already been used to register. Please use a different one";
			break;
		default:
			messageToShow = "Unknown response. Please consider filing a bug";
		}

		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		alertBuilder.setMessage(messageToShow);
		AlertDialog alert = alertBuilder.create();
		alert.show();

		// Send intent to WebView to show the response
		// Intent intent = new Intent(this.getActivity(),
		// ResponseActivity.class);
		// intent.putExtra("response", responseContent);
		// startActivity(intent);
	}

	/**
	 * Executes a HttpPost and calls function with result
	 * 
	 * @author killjoy
	 * 
	 */
	public class SendPost extends AsyncTask<HttpPost, Integer, String> {
		// ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progressDialog = ProgressDialog.show(,"Please wait...",
			// "Retrieving data ...", true);
		}

		@Override
		protected String doInBackground(HttpPost... params) {

			HttpResponse response = null;
			String responseContent = null;
			try {

				HttpClient httpClient = new DefaultHttpClient();
				// params[0].setHeader("Referer", "saarang.org");
				// params[0].setHeader("X-CSRFToken", csrfToken);

				// final BasicCookieStore cookieStore = new BasicCookieStore();

				// BasicClientCookie csrf_cookie = new
				// BasicClientCookie("csrftoken", csrfToken);
				// csrf_cookie.setDomain(CSRF_COOKIE_DOMAIN);
				// cookieStore.addCookie(csrf_cookie);

				// Create local HTTP context - to store cookies
				// HttpContext localContext = new BasicHttpContext();
				// Bind custom cookie store to the local context
				// localContext.setAttribute(ClientContext.COOKIE_STORE,
				// cookieStore);

				response = httpClient.execute(params[0]);
				responseContent = EntityUtils.toString(response.getEntity());

				Log.v("Response registration", responseContent);
			} catch (ClientProtocolException e) {
				// process exception
			} catch (IOException e) {
				// process exception
			} catch (ParseException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			return responseContent;
		}

		protected void onPostExecute(String responseContent) {
			super.onPostExecute(responseContent);
			progressBar.setVisibility(View.INVISIBLE);
			renderResponse(responseContent);
		}

		protected void onProgressUpdate(Integer... progress) {
			// progressBar.setProgress(progress[0]);
		}
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		int evX = (int) event.getX();
		int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}
	
}