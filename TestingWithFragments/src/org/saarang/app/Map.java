package org.saarang.app;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class Map extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		String imageUrl =  "file:///android_asset/saarang_map.jpg";
        WebView wv = (WebView) findViewById(R.id.wv);
        wv.getSettings().setBuiltInZoomControls(true);
        wv.loadUrl(imageUrl);
	}	
}