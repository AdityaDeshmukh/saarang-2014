package org.saarang.app;



import android.app.Fragment;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Deepest fragment, displays full details for an event.
 * Calls the interface's callbacks on clicking either EventRegister or TeamRegister, based on whether it is a team event.
 * @author snugghash
 *
 */
public class AboutEvent extends Fragment implements OnTabChangeListener,
		OnClickListener, OnTouchListener, OnItemClickListener {

	private static final String GET_FRAGMENT = "frag";
	private static final String GET_TITLE = "title";
	private static final String GET_ABOUT = "about";
	private static final String GET_ISTEAM = "isteam";
	//private static final String GET_PRIZES = "prizes";
	private static final String GET_REG = "reg";
	private static final String GET_NAMES = "names";
	private static final String GET_MOBILES = "mobiles";
	private static final String GET_IDOFEVENT = "idofevent";

	private TextView titleText, aboutText, registerText;
	//private TextView prizes;
	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	Button toEventRegButton;
	Button toCreateTeamButton;
	private ListView contacts;
	private View v;
	private String idOfEvent;
	private boolean isTeam;
	private TabHost tabHost;
	private SetLayout sl;
	int frag;
	ScrollView tab1, tab2;
	RelativeLayout tab3;
	String mobileList[];
	ArrayAdapter<String> adapter;
	ImageList cAdapter;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		v = inflater.inflate(R.layout.about_event, container, false);
		tabHost = (TabHost) v.findViewById(android.R.id.tabhost);
		tab1 = (ScrollView) v.findViewById(R.id.tab1);
		tab2 = (ScrollView) v.findViewById(R.id.tab2);
		tab3 = (RelativeLayout) v.findViewById(R.id.tab3);
		titleText = (TextView) v.findViewById(R.id.inside_info_tv);
		aboutText = (TextView) v.findViewById(R.id.textOfTab1);
		registerText = (TextView) v.findViewById(R.id.textOfTab2);
		toEventRegButton = (Button) v.findViewById(R.id.toEventRegButton);
		toCreateTeamButton = (Button) v.findViewById(R.id.toCreateTeamButton);
		contacts = (ListView) v.findViewById(R.id.textOfTab3);

		toEventRegButton.setOnClickListener(this);
		toCreateTeamButton.setOnClickListener(this);
		return v;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		setRetainInstance(true);
		comm = (Communicator) getActivity();
		db = new DiskButtons(getActivity().getApplicationContext());
		choose = (ImageView) getActivity().findViewById(R.id.disk);
		choose.setOnTouchListener(this);

		Bundle data = getArguments();
		frag = data.getInt(GET_FRAGMENT);
		idOfEvent = data.getString(GET_IDOFEVENT);
		Log.v("debug", idOfEvent);
		mobileList = data.getStringArray(GET_MOBILES);
		isTeam = data.getBoolean(GET_ISTEAM);
		//Log.v("debug", isTeam+"");
		if(isTeam==false) {
			toCreateTeamButton.setVisibility(View.GONE);
			registerText.setVisibility(View.GONE);
			//registerText.setText("This is a solo event.");
		}
		else {
			toEventRegButton.setVisibility(View.GONE);
		}
		
		sl = new SetLayout(getActivity().getApplicationContext(),
				(RelativeLayout) getActivity().findViewById(R.id.aboutpage),
				tabHost, titleText, tab1, tab2, tab3, frag);
		sl.setupLayout();
		titleText.setText(data.getString(GET_TITLE));
		String[] list = data.getStringArray(GET_NAMES);
		if (list != null) {
			cAdapter = new ImageList(getActivity(), list, R.drawable.call);
			//adapter = new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1, list);
			contacts.setAdapter(cAdapter);
		}
		contacts.setOnItemClickListener(this);
		aboutText.setText(data.getString(GET_ABOUT));
		// prizes.setText(data.getString(GET_PRIZES));
		//registerText.setText(data.getString(GET_REG));
		registerText.setText("This is a team event.");
		tabHost.setOnTabChangedListener(this);
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// Log.v("pivotTabs debug", "in onTouch");
		int action = event.getAction();
		int evX = (int) event.getX();
		int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}

	@Override
	public void onTabChanged(String str) {
		sl.tabChanged(str);
	}

	@Override
	public void onClick(View v) {
		MainActivity mainActivity = (MainActivity) getActivity();
		if (v.getId() == R.id.toEventRegButton) {
			if (mainActivity.loggedIn == false) {
				Toast.makeText(getActivity().getApplicationContext(),
						"Please login before registering", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			comm.respondToButton(4, idOfEvent, isTeam);
		}
		if (v.getId() == R.id.toCreateTeamButton) {
			if (mainActivity.loggedIn == false) {
				Toast.makeText(getActivity().getApplicationContext(),
						"Please login before registering", Toast.LENGTH_SHORT)
						.show();
				return;
			}
			comm.respondToButton(5, idOfEvent, true);
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
		try {
			Intent callIntent = new Intent(Intent.ACTION_DIAL);
			callIntent.setData(Uri.parse("tel:+91" + mobileList[arg2]));
			startActivity(callIntent);
		} catch (ActivityNotFoundException e) {
			e.printStackTrace();
		}
	}
}