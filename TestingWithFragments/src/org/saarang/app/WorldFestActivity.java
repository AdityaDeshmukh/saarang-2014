package org.saarang.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class WorldFestActivity extends Activity {

	String[] line = { "Abaji Liens", "Battle Of Life", "Byron", "Jack and Rai",
			"Murray Molloy", "Orkestar Trio", "Realta", "Respito",
			"Robin Sukroso", "WTKAS", "Grimus" };
	String[] desc = {
			"Oriental One-Man-Band (France) \n Ever heard of a Frenchman sing in Arabic? Abaji is more than just that.",
			"An Indo-German Music-Dance Rhapsody (Germany) \n B-boy Airdit GJikaj, Adam Craig (German) and Anil Srinivasan (Classical)..",
			"A 4-piece outfit playing a mix of alternative and progressive rock.",
			" Simple, Earnest, and Effective. 100% Feel-Good (Singapore)\n Jack and Rai. ",
			" A daredevil Sword-Swallower all set to thrill you (Spain) \n Ever wanted to witness a man swallow an entire sword right through his gut? ",
			"3 piece band with a Traditional-Modern Mash up (Singapore) \n The OrkeStar Trio, a mash-up of extremely diverse musical styles and loads of energy.",
			" Traditional Irish folk (Ireland) \nThe vibrant mix of Uilleann pipes, guitar, bouzouki and bohran await you.",
			"Modern Alternative Rock (Indonesia) \n For all those who can’t seem to get enough of rock, Respito is the band to watch out for.",
			"One-Man Band with a mix of electro-acoustic music (Germany)\n",
			"Hip-Hop Rock Band (Singapore) \nThis Singaporean 6 piece alternative-hip-hop rock band goes by the name of WhileTheKidsAreSurreal. ",
			"The opening show for Architects. \nActive from 2005, they have grown to be one of the most respected bands of Romania. " };
	Integer[] images = { R.drawable.wcs1, R.drawable.wcs2, R.drawable.wcs3,
			R.drawable.wcs4, R.drawable.wcs5, R.drawable.wcs6, R.drawable.wcs7,
			R.drawable.wcs8, R.drawable.wcs9, R.drawable.wcs10,
			R.drawable.wcs11 };

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_world_fest);
		// Show the Up button in the action bar.
		setupActionBar();

		ListView listView = (ListView) findViewById(R.id.world_fest_listview);
		listView.setAdapter(new WfAdapter(this, line, desc, images));
	}

	/**
	 * Set up the {@link android.app.ActionBar}.
	 */
	private void setupActionBar() {

		getActionBar().setDisplayHomeAsUpEnabled(true);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.world_fest, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}