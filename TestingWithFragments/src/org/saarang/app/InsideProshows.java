package org.saarang.app;

import android.app.Fragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class InsideProshows extends Fragment implements OnTouchListener,
		OnClickListener {
	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	boolean wait2 = false;
	TextView tv, tvh;
	ListView lv;
	ImageView iv;
	ArrayAdapter<String> adapter;
	String GET_PROSHOW = "proshow";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater
				.inflate(R.layout.inside_proshows, container, false);
		tv = (TextView) view.getRootView()
				.findViewById(R.id.inside_proshows_tv);
		tvh = (TextView) view.getRootView().findViewById(
				R.id.inside_proshows_headline);
		iv = (ImageView) view.getRootView().findViewById(R.id.proshow_poster);
		iv.setOnClickListener(this);
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		db = new DiskButtons(getActivity().getApplicationContext());
		choose = (ImageView) getActivity().findViewById(R.id.disk);

		choose.setOnTouchListener(this);
		comm = (Communicator) getActivity();

		Bundle data = getArguments();
		int proshow = data.getInt(GET_PROSHOW);
		Log.v("proshow", String.valueOf(proshow));
		switch (proshow) {
		case 1:
			tvh.setText("Classical Night");
			tv.setText("The prodigal Violin duo, fondly addressed as Ganesh-Kumaresh, took to the violin like a fish to water. Having delivered over a hundred performances before the tender age of ten, they moved on from strength to strength, emerging as one of the finest violinists of today. Popularly recognized for their refreshingly entertaining and interactive concerts, Ganesh-Kumaresh will feature in Classical Night, Saarang 2014 - treating the audience to an evening of pure delight and magic.\n\nDr. Sonal Mansingh is widely hailed as Legendary Indian Classical Dancers of today. Having started her dancing career at the age of seven, she moved from strength to strength, to master the dance forms and carve a niche for herself. With a colossal dancing experience of over 50 years behind her, Sonal Mansingh has traveled widely across the globe, honoured with many awards, including the Padma Bhushan, Sangeet Natak Akademi Award, the Padma Vibhushan among others. Come 2014, Classical Night offers all dance lovers with an unbelievable chance to witness this legend in her full glory, right here at Chennai.");
			tv.setVisibility(View.VISIBLE);
			// TODO Classical night poster
			break;
		case 2:
			tv.setVisibility(View.VISIBLE);
			tvh.setText("Choreo Night");
			tv.setText("Choreo Night being the biggest inter-collegiate group dance competition in South India, is a platform for the nation's best dance crews to show their moves. Watch them set the stage ablaze in a night of mind blowing entertainment and immense fun. The proceeds from this night will go to charity. Be there to experience one of the best performances of your life and support the cause.");
			// TODO
			break;
		case 3:
			tvh.setText("Rockshow");
			// tv.setText("Architects are a 4-piece English band from Brighton. Primarily a metal-core band, Architects are also proficient in progressive metal, mathcore and post-hardcore genres.\n\nThe past decade of their existence saw the release of 5 albums and one split up with Dead Swans, with their sixth album recently released. They are all set to release a film -- One Hundred Days: The Story of Architects Almost World Tour- about the 100 day tour for which funding was completely raised from loyal fans. The band has played in some of the biggest festivals in the world, like Download, Summer Breeze, Resurrection Fest, Grasspop Metal Meeting, Sonisphere Festival, and Hellfest, each boasting five-figure audiences, and has toured in some of the most coveted tours like the Vans Wraped Tour, with some of the biggest names in the metal world like Deftones, August Burns Red, Enter Shikari, Whitechapel, and All That Remains, to name but a few.\n\nReferred to as 'pumped with both controlled rage and unhindered heart, accessible and ambitious, aggressive and beautiful', Architects have performed at various places all over the world, including America, Canada, Germany, England, Malaysia, Philippines and other countries of Europe, Australia and Asia, making the crowd go berserk in every performance. This January, Architects will be enthralling the Indian audience for the first ever time, headlining the Saarang 2014 Rockshow. This will be one great show for not just metal lovers, but for anyone who appreciate some good music!\n\nOpening for Architects is an alternative band from Romania. Active from 2005, they have grown to be one of the most respected bands of Romania. With the honor of opening on arenas for Placebo and Red Hot Chili Peppers in Bucharest, as well as Roxette in their hometown, Cluj, they are not new to live performances and are ready to rock the stage wherever they are.");
			// iv.setScaleType(ScaleType.FIT_CENTER);
			iv.setScaleX(1.25f);
			iv.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.rockshow_poster));
			break;
		case 4:
			tvh.setText("EDM Night");
			// tv.setText("Coming January 10th, Chennai will witness one of its largest parties with Sunburn Campus at IIT Madras' 5000 strong venue, the Open Air Theatre, for the first time at Saarang.\n\nWith three headlining acts, all making waves in the scene across the country, taking the same stage for the first time ever, the night promises to triple the party.\n\nStarting from the now country wide sensation from last year, Dualist Inquiry will bring in Guitar-driven Electro rock, the pioneer of his one-of-a-kind sound, Nucleya will be dropping Dubstep and Desi Bass, and the party will end with the Sunburn headlining duo Lost Stories x Anish Sood spinning their chart-topping mashups and Sunburn smashing house. The night will be opened by DJ Ankur Sood.\n\nThree different genres, one night.\n\nChennai, this is one party you don't want to miss!\n\nDualist Inquiry -- Guitar-driven electro rock\n\nNucleya -- Dubstep, Bass, Desi\n\nLost Stories X Anish Sood -- House\n\nDJ Ankur Sood -- Techno/House");
			iv.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.edm_poster));
			break;
		case 5:
			tvh.setText("Popular Night");
			// tv.setText("Sonu Nigam, Shankar-Ehsaan-Loy, Vishal-Shekhar.\n\nAnd now, Saarang presents to you Salim - Sulaiman -- the musician brothers who have gifted us with numbers for movies such as Rab Ne Bana Di Jodi, Chak De! India, Fashion, Heroine, Ladies vs. Ricky Bahl, Band Baaja Baaraat among others.\n\nThe dynamic duo will set the \"Popular Night\" alive for you! Open Air Theatre IIT Madras is the place to be on the 12th of January 2014 to witness yet another masterclass!\n\nSaarang always has a little extra for you! The night will also witness a performance by Benny Dayal, the South Indian sensation. After all, who can resist to dance to songs such as 'Pappu can't dance saala' and 'Badtameez dil' !\n\nSo, are you ready to make some noise this January?");
			iv.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.pop_poster));
			break;
		default:
			tvh.setText("lololol");
		}

	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		int evX = (int) event.getX();
		int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		String url = "http://www.saarang.org/tickets";
		Intent i = new Intent(Intent.ACTION_VIEW);
		i.setData(Uri.parse(url));
		startActivity(i);
	}
}
