package org.saarang.app;



import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;
import android.widget.ImageView;

public class DiskButtons{
	
	private static final int HOME = 1;
	private static final int SPOTLIGHT = 2;
	private static final int EVENTS = 3;
	private static final int PROSHOWS = 4;
	private static final int INFORMALS = 5;
	private static final int SEARCH = 6;
	private Communicator comm;
	private Context context;
	
	DiskButtons(Context c){
		this.context = c;
	}
	
	public int getHotspotColor(ImageView img, int x, int y) {
		img.setDrawingCacheEnabled(true); 
		Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
		img.setDrawingCacheEnabled(false);
		if(y>=0&&x>=0&&y<hotspots.getHeight()&&x<hotspots.getWidth())
			return hotspots.getPixel(x, y);
		else
			return -1;
	}
	public boolean closeMatch(int color1, int color2, int tolerance) {
		if ((int) Math.abs (Color.red (color1) - Color.red (color2)) > tolerance )
		    return false;
		if ((int) Math.abs (Color.green (color1) - Color.green (color2)) > tolerance )
		    return false;
		if ((int) Math.abs (Color.blue (color1) - Color.blue (color2)) > tolerance )
			return false;
		return true;
	}
	public void checkButton(Communicator communicator, ImageView iv, int evX, int evY){
		this.comm=communicator;
		int touchColor = getHotspotColor (iv, evX, evY);
		if(touchColor!=-1){
			//Vibrator vb = (Vibrator)activity.getSystemService(Context.VIBRATOR_SERVICE);
            //vb.vibrate(50);
			
			int tolerance = 25;
			
			Log.v("PivotTabs debug", "in checkButton" + touchColor + " " + tolerance);
			if (closeMatch (Color.RED, touchColor, tolerance)) comm.respond(HOME);
			else if(closeMatch (Color.GREEN, touchColor, tolerance)){Log.v("PivotTabs debug", "in checkButton" + touchColor + " " + tolerance);
			 comm.respond(SPOTLIGHT);}
			else if(closeMatch (Color.BLUE, touchColor, tolerance)) comm.respond(EVENTS);
			else if(closeMatch (Color.YELLOW, touchColor, tolerance)) comm.respond(PROSHOWS);
			else if(closeMatch (Color.MAGENTA, touchColor, tolerance)) comm.respond(INFORMALS);
			else if(closeMatch (Color.CYAN, touchColor, tolerance)) comm.respond(SEARCH);
		}
	}
	public void changeImage(ImageView disk, ImageView iv, int evX, int evY){
		int touchColor = getHotspotColor(iv, evX, evY);
		if(touchColor!=-1){
			int tolerance = 25;
			if (closeMatch (Color.RED, touchColor, tolerance)) disk.setImageDrawable(context.getResources().getDrawable(R.drawable.home_pressed));
			else if(closeMatch (Color.GREEN, touchColor, tolerance)) disk.setImageDrawable(context.getResources().getDrawable(R.drawable.spotlight_pressed));
			else if(closeMatch (Color.BLUE, touchColor, tolerance)) disk.setImageDrawable(context.getResources().getDrawable(R.drawable.events_pressed));
			else if(closeMatch (Color.YELLOW, touchColor, tolerance)) disk.setImageDrawable(context.getResources().getDrawable(R.drawable.proshows_pressed));
			else if(closeMatch (Color.MAGENTA, touchColor, tolerance)) disk.setImageDrawable(context.getResources().getDrawable(R.drawable.informals_pressed));
			else if(closeMatch (Color.CYAN, touchColor, tolerance)) disk.setImageDrawable(context.getResources().getDrawable(R.drawable.search_pressed));
		}
	}
}