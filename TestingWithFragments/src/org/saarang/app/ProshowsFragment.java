package org.saarang.app;



import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class ProshowsFragment extends Fragment implements OnTouchListener,
		OnClickListener {

	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	private ImageView classical, choreo, popular, edm, rockshow, wf;
	// TODO Clear up id
	private static final int id = 14;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.proshowsfrag, container, false);
		
		classical = (ImageView) view.getRootView().findViewById(R.id.classicalnight);
		choreo = (ImageView) view.getRootView().findViewById(R.id.choreo);
		popular = (ImageView) view.getRootView().findViewById(R.id.popularnight);
		rockshow = (ImageView) view.getRootView().findViewById(R.id.rockshow);
		edm = (ImageView) view.getRootView().findViewById(R.id.edm);
		wf = (ImageView) view.getRootView().findViewById(R.id.wf);
		
		classical.setOnClickListener(this);
		choreo.setOnClickListener(this);
		popular.setOnClickListener(this);
		edm.setOnClickListener(this);
		rockshow.setOnClickListener(this);
		wf.setOnClickListener(this);
		
		return view;
	}

	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);

		db = new DiskButtons(getActivity().getApplicationContext());

		choose = (ImageView) getActivity().findViewById(R.id.disk);
		choose.setOnTouchListener(this);
		comm = (Communicator) getActivity();
	}

	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		if (nextAnim != 0) {
			Animator anim = AnimatorInflater.loadAnimator(getActivity(),
					nextAnim);
			anim.addListener(new AnimatorListenerAdapter() {
				@Override
				public void onAnimationStart(Animator animation) {
					wait = true;
				}

				@Override
				public void onAnimationEnd(Animator animation) {
					wait = false;
				}
			});
			return anim;
		} else
			return null;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		final int action = event.getAction();
		final int evX = (int) event.getX();
		final int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}

	@Override
	public void onClick(View v) {
		if (v.getId() == R.id.classicalnight)
			comm.respondProshow(id, 1);
		else if (v.getId() == R.id.choreo)
			comm.respondProshow(id, 2);
		else if (v.getId() == R.id.rockshow)
			comm.respondProshow(id, 3);
		else if (v.getId() == R.id.edm)
			comm.respondProshow(id, 4);
		else if (v.getId() == R.id.popularnight)
			comm.respondProshow(id, 5);
		else if(v.getId() == R.id.wf)
			comm.respondProshow(id, 6);
	}
}