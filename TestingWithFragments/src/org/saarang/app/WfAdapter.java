package org.saarang.app;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

public class WfAdapter extends ArrayAdapter<String> {
	private final Activity context;
	private final String[] web;
	private final String[] small;
	private final Integer[] imageId;

	public WfAdapter(Activity context, String[] web, String[] small,
			Integer[] imageId) {
		super(context, R.layout.world_fest_ele, web);
		this.context = context;
		this.web = web;
		this.small = small;
		this.imageId = imageId;
	}

	@Override
	public View getView(int position, View view, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		View rowView;
		if (position % 2 == 0) {
			rowView = inflater.inflate(R.layout.world_fest_ele, null, true);
		} else {
			rowView = inflater.inflate(R.layout.world_fest_ele2, null, true);	
		}
		//ScrollView sl = (ScrollView) rowView.findViewById(R.id.scoll);
		TextView txtbig = (TextView) rowView.findViewById(R.id.wftxtbig);
		TextView txtsmall = (TextView) rowView.findViewById(R.id.wftxtsmall);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.wfimg);
		txtbig.setText(web[position]);
		imageView.setImageResource(imageId[position]);
		txtsmall.setText(small[position]);
		
		// Setting alt image and text
//			imageView.setLeft(0);
//			txtbig.setRight(0);
//			txtsmall.setLeft(0);
//			
		
		return rowView;
	}
}