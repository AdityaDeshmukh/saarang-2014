package org.saarang.app;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

public class SpotlightFragment extends Fragment implements OnTouchListener, OnClickListener{
	
	private static final int id = 12;
	
	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	boolean wait2 = false;
	ImageButton decibels_sp,dramatics_sp,taarang_sp,fmi_sp,panache_sp,pitstop_sp,power_sp,road_sp,zone_sp;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		return inflater.inflate(R.layout.spotlightfrag, container, false);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		db = new DiskButtons(getActivity().getApplicationContext());
		
		choose = (ImageView) getActivity().findViewById(R.id.disk);
		choose.setOnTouchListener(this);
		
		decibels_sp = (ImageButton) getActivity().findViewById(R.id.spot_decibels);
		dramatics_sp = (ImageButton) getActivity().findViewById(R.id.spot_dramatics);
		taarang_sp = (ImageButton) getActivity().findViewById(R.id.spot_taarang);
		fmi_sp = (ImageButton) getActivity().findViewById(R.id.spot_fmi);
		panache_sp = (ImageButton) getActivity().findViewById(R.id.spot_panache);
		pitstop_sp = (ImageButton) getActivity().findViewById(R.id.spot_pitstop);
		power_sp = (ImageButton) getActivity().findViewById(R.id.spot_power);
		road_sp = (ImageButton) getActivity().findViewById(R.id.spot_road);
		zone_sp = (ImageButton) getActivity().findViewById(R.id.spot_zone);
		
		decibels_sp.setOnClickListener(this);
		dramatics_sp.setOnClickListener(this);
		taarang_sp.setOnClickListener(this);
		fmi_sp.setOnClickListener(this);
		panache_sp.setOnClickListener(this);
		pitstop_sp.setOnClickListener(this);
		power_sp.setOnClickListener(this);
		road_sp.setOnClickListener(this);
		zone_sp.setOnClickListener(this);

		comm=(Communicator) getActivity();
		
		final Animation anim = AnimationUtils.loadAnimation(getActivity(), R.animator.anim_button);
		decibels_sp.startAnimation(anim);
		dramatics_sp.startAnimation(anim);
		taarang_sp.startAnimation(anim);
		fmi_sp.startAnimation(anim);
		panache_sp.startAnimation(anim);
		pitstop_sp.startAnimation(anim);
		power_sp.startAnimation(anim);
		road_sp.startAnimation(anim);
		zone_sp.startAnimation(anim);	
	}
	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		if(nextAnim!=0){
			Animator anim = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
	        anim.addListener(new AnimatorListenerAdapter() {
	        	@Override
	            public void onAnimationStart(Animator animation) {
	        		wait=true;
	            }
	            @Override
	            public void onAnimationEnd(Animator animation) {
	            	wait=false;
	            }
	        });
	        return anim;
		}
		else
			return null;
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		final int action = event.getAction();
		final int evX = (int) event.getX();
		final int evY = (int) event.getY();
		switch (action) {
			case MotionEvent.ACTION_DOWN :
			case MotionEvent.ACTION_MOVE:
					db.changeImage(choose,(ImageView)getActivity().findViewById(R.id.mask), evX, evY);
				break;
			case MotionEvent.ACTION_UP :
				choose.setImageDrawable(getActivity().getResources().getDrawable(R.drawable.disk_unpressed));
				if(!wait)
					db.checkButton(comm, (ImageView)getActivity().findViewById(R.id.mask), evX, evY);
				break;
		}
		return true;
	}
	@Override
	public void onClick(View v) {
			if(v.getId()==R.id.spot_decibels) comm.respondInsideEvent(id, 15);
			else if(v.getId()==R.id.spot_dramatics) comm.respondInsideEvent(id, 18);
			else if(v.getId()==R.id.spot_taarang) comm.respondInsideEvent(id, 50);
			else if(v.getId()==R.id.spot_fmi) comm.respondInsideEvent(id, 91);
			else if(v.getId()==R.id.spot_panache) comm.respondInsideEvent(id, 32);
			else if(v.getId()==R.id.spot_pitstop) comm.respondInsideEvent(id, 54);
			else if(v.getId()==R.id.spot_power) comm.respondInsideEvent(id, 35);
			else if(v.getId()==R.id.spot_road) comm.respondInsideEvent(id, 36);
			else if(v.getId()==R.id.spot_zone) comm.respondInsideEvent(id, 3);
	}
}
