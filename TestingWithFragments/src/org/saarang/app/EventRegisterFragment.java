package org.saarang.app;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import com.parse.PushService;

import android.app.AlertDialog;
import android.app.Fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

/**
 * Registers a user for an event, gets event_id from Bundle. 
 * @author snugghash
 *
 */
public class EventRegisterFragment extends Fragment implements OnClickListener,OnTouchListener {

	private ProgressBar progressBar;
	private Button eventReg;
	private long idOfEvent;
	Communicator comm;
	ImageView choose;
	DiskButtons db;
	boolean wait = false;
	TextView evRegIns;
	String eventId;
	private boolean isTeam;
	private static final String GET_IDOFEVENT = "idofevent";
	private static final String GET_ISTEAM = "isteam";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_eventreg, container,
				false);

		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);
		
		// Check whether Network is availabile
		if (isNetworkAvailable() == false) {
			Toast.makeText(getActivity(),
					"Please try again when network is available",
					Toast.LENGTH_LONG).show();
			return view;
		}
		
		evRegIns = (TextView) view.findViewById(R.id.evregins);
		eventReg = (Button) view.findViewById(R.id.eventreg_button);
		eventReg.setOnClickListener(this);
		return view;
	}
	
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		db = new DiskButtons(getActivity().getApplicationContext());
		choose = (ImageView) getActivity().findViewById(R.id.disk);

		choose.setOnTouchListener(this);
		comm = (Communicator) getActivity();
		
		Bundle data = getArguments();
		//idOfEvent = data.getLong(GET_IDOFEVENT);
		//eventId = String.valueOf(idOfEvent);
		eventId = data.getString(GET_IDOFEVENT);
		//Log.v("debug title 2 ", data.getString("title") + " ");
		isTeam = data.getBoolean(GET_ISTEAM);
		if(isTeam==false) {
			evRegIns.setText("Click on 'Register for this event'");
		}
	}

	@Override
	public void onClick(View v) {
		if (v == null) {
			Toast.makeText(getActivity(),
					"View is null for some reason! Contact the developer",
					Toast.LENGTH_SHORT).show();
			return;
		}

		String deviceId = Secure.getString(getActivity()
				.getApplicationContext().getContentResolver(),
				Secure.ANDROID_ID);
		//String eventId = ((EditText) v.getRootView()
		//		.findViewById(R.id.event_id)).getText().toString();
//		String teamNameString = ((EditText) v.getRootView().findViewById(R.id.team_name)).getText().toString();
//		if(teamName.length()==0 && isTeam) {
//			Toast.makeText(getActivity().getApplicationContext(), "Please enter team name", Toast.LENGTH_SHORT).show();
//		}
		

		String hashedKey = deviceId.substring(deviceId.length()-4, deviceId.length()) + deviceId.substring(0, deviceId.length()-4);
		Log.v("debug new hash", hashedKey);
		
		// Sending to an AsyncTask to POST data
		HttpPost httpPost = new HttpPost(
				"http://erp.saarang.org/mobile/register_event/");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("key", hashedKey));
		Log.v("debug idof evnt2", ""+eventId);
		nameValuePairs.add(new BasicNameValuePair("event_id", eventId));
		//if(isTeam)nameValuePairs.add(new BasicNameValuePair("team_name", teamNameString));
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		progressBar.setVisibility(View.VISIBLE);
		new SendPost().execute(httpPost);
	}

	/**
	 * Executes a HttpPost and calls function with result
	 * 
	 * @author killjoy
	 * 
	 */
	public class SendPost extends AsyncTask<HttpPost, Integer, String> {
		// ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progressDialog = ProgressDialog.show(,"Please wait...",
			// "Retrieving data ...", true);
		}

		@Override
		protected String doInBackground(HttpPost... params) {

			HttpResponse response = null;
			String responseContent = null;
			try {

				HttpClient httpClient = new DefaultHttpClient();
				response = httpClient.execute(params[0]);
				responseContent = EntityUtils.toString(response.getEntity());

				Log.v("Response registration", responseContent);
			} catch (ClientProtocolException e) {
				// process exception
			} catch (IOException e) {
				// process exception
			} catch (ParseException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			return responseContent;
		}

		protected void onPostExecute(String responseContent) {
			super.onPostExecute(responseContent);
			progressBar.setVisibility(View.INVISIBLE);
			//TODO LOG
			renderResponse(responseContent);
		}

		protected void onProgressUpdate(Integer... progress) {
			// progressBar.setProgress(progress[0]);
		}
	}

	public void renderResponse(String responseContent) {
		String messageToShow;
		if (responseContent == null) {
			messageToShow = "No response from the server, check your internet connection.";
		} else {
			// Display dialogues for each response
			// char responseChar =
			// responseContent.charAt(responseContent.indexOf("<body>")+7);
			char responseChar = responseContent.charAt(0);
			Log.v("responseChar", String.valueOf(responseChar));

			switch (responseChar) {
			case 'S':
				messageToShow = "You have successfully registered for this event!\nCheck your email for confirmation.";
				//Subscribe to Parse channel "event_+(event_id.toString())"
				PushService.subscribe(getActivity().getApplicationContext(), "event_" + eventId, ReceivePush.class);
				break;
			case 'T':
				messageToShow = "This is a team event. Please create a new team, or type in the name of an existing one";
				break;
			case 'N':
				messageToShow = "Email not registered or activated, please register first.";
				break;
			case 'A':
				PushService.subscribe(getActivity().getApplicationContext(), "event_" + eventId, ReceivePush.class);
				messageToShow = "You have already registered for this event.";
				break;
			case 'C':
				messageToShow = "Registrations are closed for this event.";
				break;
			default:
				messageToShow = "Unknown response. Please consider filing a bug.";
			}
		}
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		alertBuilder.setMessage(messageToShow);
		AlertDialog alert = alertBuilder.create();
		alert.show();

		//Send intent to WebView to show the response
//		Intent intent = new Intent(this.getActivity(),
//		ResponseActivity.class);
//		intent.putExtra("response", responseContent);
//		startActivity(intent);
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
	
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		int action = event.getAction();
		int evX = (int) event.getX();
		int evY = (int) event.getY();
		switch (action) {
		case MotionEvent.ACTION_DOWN:
		case MotionEvent.ACTION_MOVE:
			db.changeImage(choose,
					(ImageView) getActivity().findViewById(R.id.mask), evX, evY);
			break;
		case MotionEvent.ACTION_UP:
			choose.setImageDrawable(getActivity().getResources().getDrawable(
					R.drawable.disk_unpressed));
			if (!wait)
				db.checkButton(comm,
						(ImageView) getActivity().findViewById(R.id.mask), evX,
						evY);
			break;
		}
		return true;
	}
}
