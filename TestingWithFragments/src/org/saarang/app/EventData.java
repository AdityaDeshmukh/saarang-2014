package org.saarang.app;

import android.database.Cursor;

/*
 * Stores the details for the event.
 * Used as container to transfer data from the db to the eventInfopage
 */
public class EventData {
	public String name, group, description, prizes, timeLocation;
	public EventData() {
		
	}
	
	//Don't use this as we will need to change here also when ever there is change in db design.
	public EventData(Cursor mCursor) {
		
		name = mCursor.getString(1);
		description = mCursor.getString(2);
	}

}
