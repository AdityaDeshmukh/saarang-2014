package com.example.tabfragmenttest;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.ImageView;

public class EventsFragment extends Fragment implements OnTouchListener{
	Communicator comm;
	ImageView choose;
	DiskButtons db = new DiskButtons();
	boolean wait = false;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.eventsfrag, container, false);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		choose = (ImageView) getActivity().findViewById(R.id.pivot);
		choose.setOnTouchListener(this);
		comm=(Communicator) getActivity();
	}
	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		// TODO Auto-generated method stub
		if(nextAnim!=0){
			Animator anim = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
	        anim.addListener(new AnimatorListenerAdapter() {
	        	@Override
	            public void onAnimationStart(Animator animation) {
	        		wait=true;
	            }
	            @Override
	            public void onAnimationEnd(Animator animation) {
	            	wait=false;
	            }
	        });
	        return anim;
		}
		else
			return null;
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		final int action = event.getAction();
		final int evX = (int) event.getX();
		final int evY = (int) event.getY();
		switch (action) {
			case MotionEvent.ACTION_DOWN :
				break;
			case MotionEvent.ACTION_UP :
				if(!wait)
					db.checkButton(comm,(ImageView)getActivity().findViewById(R.id.mask), evX, evY);
				break;
		}
		return true;
	}
}
