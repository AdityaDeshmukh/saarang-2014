package com.example.tabfragmenttest;

import android.annotation.SuppressLint;
import android.support.v4.view.ViewPager.PageTransformer;
import android.view.View;

/**
 * Based on offset from position, scales, fades and rotates the fragments.
 * 	
 * 
 * @author killjoy
 *
 */

public class ZoomOutPivotPageTransformer implements PageTransformer {

	private static float MIN_SCALE = 0.85f;
	//private static float MIN_ALPHA = 0.5f;
	private static float FIN_ROTATION = 45;

    @SuppressLint("NewApi")
	@Override
    public void transformPage(View view, float position) {
        int pageWidth = view.getWidth();
        int pageHeight = view.getHeight();

        if (position <= -1) { // [-Infinity,-1)
            // This page is way off-screen to the left.
            view.setAlpha(0);

        } else if (position < 1) { // [-1,1]
            // Modify the default slide transition to shrink the page as well
            float scaleFactor = Math.max(MIN_SCALE, 1 - Math.abs(position));
               
            //Rotation about bottom of the screen
        	view.setPivotY(pageHeight);
        	view.setPivotX(pageWidth/2-position*pageWidth);
        	view.setRotation(position*FIN_ROTATION);
            
            // Scale the page down (between MIN_SCALE and 1)
            view.setScaleX(scaleFactor);
            view.setScaleY(scaleFactor);
            
            // Fade the page relative to its size.
            view.setAlpha(1-Math.abs(position));

        } else { // (1,+Infinity]
            // This page is way off-screen to the right.
            view.setAlpha(0);
        }
    }
}
