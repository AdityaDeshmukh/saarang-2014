package com.example.tabfragmenttest;

import com.nineoldandroids.animation.AnimatorSet;
import com.nineoldandroids.animation.ObjectAnimator;

import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorListenerAdapter;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

public class HomeFragment extends Fragment implements OnTouchListener{
	ImageView OrangeView, GreenView, BlueView, WhiteView;
	Communicator comm;
	ImageView choose;
	DiskButtons db = new DiskButtons();
	boolean wait = false;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		return inflater.inflate(R.layout.homefrag, container, false);
	}
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onActivityCreated(savedInstanceState);
		choose = (ImageView) getActivity().findViewById(R.id.disk);
		OrangeView = (ImageView) getActivity().findViewById(R.id.orange);
		BlueView = (ImageView) getActivity().findViewById(R.id.blue);
		GreenView = (ImageView) getActivity().findViewById(R.id.green);
		WhiteView = (ImageView) getActivity().findViewById(R.id.white);
		choose.setOnTouchListener(this);
		comm=(Communicator) getActivity();
		InitAnim();
        KeyAnim();
	}
	@Override
	public Animator onCreateAnimator(int transit, boolean enter, int nextAnim) {
		// TODO Auto-generated method stub
		if(nextAnim!=0){
			Animator anim = AnimatorInflater.loadAnimator(getActivity(), nextAnim);
	        anim.addListener(new AnimatorListenerAdapter() {
	        	@Override
	            public void onAnimationStart(Animator animation) {
	        		wait=true;
	            }
	            @Override
	            public void onAnimationEnd(Animator animation) {
	            	wait=false;
	            }
	        });
	        return anim;
		}
		else
			return null;
	}
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		// TODO Auto-generated method stub
		final int action = event.getAction();
		final int evX = (int) event.getX();
		final int evY = (int) event.getY();
		switch (action) {
			case MotionEvent.ACTION_DOWN :
				break;
			case MotionEvent.ACTION_UP :
				if(!wait)
					db.checkButton(comm,(ImageView)getActivity().findViewById(R.id.mask), evX, evY);
				break;
		}
		return true;
	}
	public void InitAnim(){
    	
    	ObjectAnimator RotatAnim = ObjectAnimator.ofFloat(OrangeView, "rotation", 150);
    	RotatAnim.setDuration(1);
    	RotatAnim.start();
    	
    	RotatAnim = ObjectAnimator.ofFloat(GreenView, "rotation", 200);
    	RotatAnim.setDuration(1);
    	RotatAnim.start();
    	
    	RotatAnim = ObjectAnimator.ofFloat(BlueView, "rotation", 300);
    	RotatAnim.setDuration(1);
    	RotatAnim.start();
    	
    	ObjectAnimator FadeAnim = ObjectAnimator.ofFloat(WhiteView, "alpha", 0);
    	FadeAnim.setDuration(1);
    	FadeAnim.start();
    }
	public void KeyAnim() {	
		
		ObjectAnimator RotatAnim1 = ObjectAnimator.ofFloat(OrangeView, "rotation", 0);
		RotatAnim1.setDuration(1500);
		RotatAnim1.setInterpolator(new DecelerateInterpolator());
		
		ObjectAnimator RotatAnim2 = ObjectAnimator.ofFloat(GreenView, "rotation", 360);
		RotatAnim2.setDuration(1500);
		RotatAnim2.setInterpolator(new DecelerateInterpolator());
		
		ObjectAnimator RotatAnim3 = ObjectAnimator.ofFloat(BlueView, "rotation", 0);
		RotatAnim3.setDuration(1500);
		RotatAnim3.setInterpolator(new DecelerateInterpolator());
		
		ObjectAnimator FadeAnim = ObjectAnimator.ofFloat(WhiteView, "alpha", 100);
		FadeAnim.setDuration(1000);
		
		AnimatorSet LockAnim = new AnimatorSet();
		
		LockAnim.play(RotatAnim1);
		LockAnim.play(RotatAnim2).after(500);
		LockAnim.play(RotatAnim3).after(1200);
		LockAnim.play(FadeAnim).after(1500);
		
		ObjectAnimator Scalex1 = ObjectAnimator.ofFloat(OrangeView, "scaleX", 2);
		ObjectAnimator Scalex2 = ObjectAnimator.ofFloat(GreenView, "scaleX", 2);
		ObjectAnimator Scalex3 = ObjectAnimator.ofFloat(BlueView, "scaleX", 2);
		ObjectAnimator Scalex4 = ObjectAnimator.ofFloat(WhiteView, "scaleX", 2);
		ObjectAnimator Scaley1 = ObjectAnimator.ofFloat(OrangeView, "scaleY", 2);
		ObjectAnimator Scaley2 = ObjectAnimator.ofFloat(GreenView, "scaleY", 2);
		ObjectAnimator Scaley3 = ObjectAnimator.ofFloat(BlueView, "scaleY", 2);
		ObjectAnimator Scaley4 = ObjectAnimator.ofFloat(WhiteView, "scaleY", 2);
		
		AnimatorSet ScaleAnim = new AnimatorSet();
		ScaleAnim.play(Scalex1).with(Scalex2).with(Scalex3).with(Scalex4).with(Scaley4).with(Scaley1).with(Scaley2).with(Scaley3);
		ScaleAnim.setInterpolator(new AccelerateDecelerateInterpolator());
		ScaleAnim.setDuration(1);
		
		ScaleAnim.start();
		LockAnim.setStartDelay(100);
		LockAnim.start();    	
	}
}