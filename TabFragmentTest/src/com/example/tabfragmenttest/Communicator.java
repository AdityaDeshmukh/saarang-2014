package com.example.tabfragmenttest;

public interface Communicator {
	public void respond(int id);
}
