package com.example.tabfragmenttest;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.widget.ImageView;

public class DiskButtons{
	Communicator comm;
	public int getHotspotColor(ImageView img, int x, int y) {
		// TODO Auto-generated method stub
		img.setDrawingCacheEnabled(true); 
		Bitmap hotspots = Bitmap.createBitmap(img.getDrawingCache());
		img.setDrawingCacheEnabled(false);
		if(y>=0)
			return hotspots.getPixel(x, y);
		else
			return -1;
	}
	public boolean closeMatch(int color1, int color2, int tolerance) {
		// TODO Auto-generated method stub
		if ((int) Math.abs (Color.red (color1) - Color.red (color2)) > tolerance )
		    return false;
		if ((int) Math.abs (Color.green (color1) - Color.green (color2)) > tolerance )
		    return false;
		if ((int) Math.abs (Color.blue (color1) - Color.blue (color2)) > tolerance )
			return false;
		return true;
	}
	public void checkButton(Communicator communicator, ImageView iv, int evX, int evY){
		comm=communicator;
		int touchColor = getHotspotColor (iv, evX, evY);
		if(touchColor!=-1){
			int tolerance = 25;
			if (closeMatch (Color.RED, touchColor, tolerance)) comm.respond(1);
			else if(closeMatch (Color.GREEN, touchColor, tolerance)) comm.respond(2);
			else if(closeMatch (Color.BLUE, touchColor, tolerance)) comm.respond(3);
			else if(closeMatch (Color.YELLOW, touchColor, tolerance)) comm.respond(4);
			else if(closeMatch (Color.MAGENTA, touchColor, tolerance)) comm.respond(5);
		}
	}
}