package com.example.dbupdatetest;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.PushService;

import android.app.Application;

public class DBUpdateTestApplication extends Application{
	@Override
	public void onCreate() {
		super.onCreate();

		// Add your initialization code here
		 Parse.initialize(this, "c39soVyRy8MRGfSQO2hLF8GQkJpVi6VqDJQB9WBS", "OUzton9WRjgk6iYt2nwv15xYsukSTXCn6W7VICNS");

		ParseUser.enableAutomaticUser();
		ParseACL defaultACL = new ParseACL();

		// If you would like all objects to be private by default, remove this
		// line.
		defaultACL.setPublicReadAccess(true);

		ParseACL.setDefaultACL(defaultACL, true);
		
		//Set callback activity on receiving a push. TODO
		PushService.setDefaultPushCallback(this, MainActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
	}
}
