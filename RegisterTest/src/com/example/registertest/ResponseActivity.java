package com.example.registertest;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.webkit.WebView;

public class ResponseActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_response);
		
		Bundle extras = getIntent().getExtras();
		String data;
		if (savedInstanceState == null) {
		    extras = getIntent().getExtras();
		    if(extras == null) {
		        data= null;
		    } else {
		        data= extras.getString("response");
		    }
		} else {
		    data= (String) savedInstanceState.getSerializable("response");
		}
		
		String mime = "text/html";
		String encoding = "utf-8";
		WebView myWebView = (WebView)this.findViewById(R.id.webView);
		myWebView.getSettings().setJavaScriptEnabled(true);
		myWebView.loadData(data, mime, encoding);
		//myWebView.loadDataWithBaseURL(null, html, mime, encoding, null);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.response, menu);
		return true;
	}

}
