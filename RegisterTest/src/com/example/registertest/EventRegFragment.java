package com.example.registertest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class EventRegFragment extends Fragment implements OnClickListener {

	private ProgressBar progressBar;
	private Button eventReg;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.fragment_eventreg, container,
				false);

		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

		// Check whether Network is availabile
		if (isNetworkAvailable() == false) {
			Toast.makeText(getActivity(),
					"Please try again when network is available",
					Toast.LENGTH_LONG).show();
			return view;
		}

		eventReg = (Button) view.findViewById(R.id.eventreg_button);
		eventReg.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		if (v == null) {
			Toast.makeText(getActivity(),
					"View is null for some reason! Contact the developer",
					Toast.LENGTH_SHORT).show();
			return;
		}

		String deviceId = Secure.getString(getActivity()
				.getApplicationContext().getContentResolver(),
				Secure.ANDROID_ID);
		String eventId = ((EditText) v.getRootView()
				.findViewById(R.id.event_id)).getText().toString();
		String teamName = ((EditText) v.getRootView().findViewById(R.id.create_team_fragment)).getText().toString();
		if(eventId.length()==0||teamName.length()==0) {
			Toast.makeText(getActivity().getApplicationContext(), "Please enter event id, team name, and at least one member", Toast.LENGTH_SHORT).show();
		}
		
		// Sending to an AsyncTask to POST data
		HttpPost httpPost = new HttpPost(
				"http://erp.saarang.org/mobile/register_event/");
		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		nameValuePairs.add(new BasicNameValuePair("key", deviceId));
		nameValuePairs.add(new BasicNameValuePair("event_id", eventId));
		nameValuePairs.add(new BasicNameValuePair("team_name", teamName));
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		progressBar.setVisibility(View.VISIBLE);
		new SendPost().execute(httpPost);
	}

	/**
	 * Executes a HttpPost and calls function with result
	 * 
	 * @author killjoy
	 * 
	 */
	public class SendPost extends AsyncTask<HttpPost, Integer, String> {
		// ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progressDialog = ProgressDialog.show(,"Please wait...",
			// "Retrieving data ...", true);
		}

		@Override
		protected String doInBackground(HttpPost... params) {

			HttpResponse response = null;
			String responseContent = null;
			try {

				HttpClient httpClient = new DefaultHttpClient();
				response = httpClient.execute(params[0]);
				responseContent = EntityUtils.toString(response.getEntity());

				Log.v("Response registration", responseContent);
			} catch (ClientProtocolException e) {
				// process exception
			} catch (IOException e) {
				// process exception
			} catch (ParseException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			return responseContent;
		}

		protected void onPostExecute(String responseContent) {
			super.onPostExecute(responseContent);
			progressBar.setVisibility(View.INVISIBLE);
			renderResponse(responseContent);
		}

		protected void onProgressUpdate(Integer... progress) {
			// progressBar.setProgress(progress[0]);
		}
	}

	public void renderResponse(String responseContent) {
		String messageToShow;
		if (responseContent == null) {
			messageToShow = "No response from the server, check your internet connection.";
		} else {
			// Display dialogues for each response
			// char responseChar =
			// responseContent.charAt(responseContent.indexOf("<body>")+7);
			char responseChar = responseContent.charAt(0);
			Log.v("responseChar", String.valueOf(responseChar));

			switch (responseChar) {
			case 'S':
				messageToShow = "You have successfully registered for this event!";
				//TODO Subscribe to Parse channel "event_+(event_id.toString())"
				break;
			case 'T':
				messageToShow = "This is a team event. Please create a new team, or type in the name of an existing one";
				break;
			case 'N':
				messageToShow = "Email not registered or activated, please register first.";
				break;
			case 'A':
				messageToShow = "You have already registered for this event.";
				break;
			case 'C':
				messageToShow = "Registrations are closed for this event.";
				break;
			default:
				messageToShow = "Unknown response. Please consider filing a bug.";
			}
		}
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		alertBuilder.setMessage(messageToShow);
		AlertDialog alert = alertBuilder.create();
		alert.show();

		//Send intent to WebView to show the response
//		Intent intent = new Intent(this.getActivity(),
//		ResponseActivity.class);
//		intent.putExtra("response", responseContent);
//		startActivity(intent);
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}
