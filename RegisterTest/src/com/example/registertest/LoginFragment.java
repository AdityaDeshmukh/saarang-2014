package com.example.registertest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings.Secure;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class LoginFragment extends Fragment implements OnClickListener {

	private ProgressBar progressBar;
	private Button signinButton;
	private String csrfToken;
	public String saarangId;

	// private final String CSRF_COOKIE_DOMAIN = "saarang.org";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		View view = inflater.inflate(R.layout.login_fragment, container, false);

		progressBar = (ProgressBar) view.findViewById(R.id.progressBar1);

		// Check whether Network is availabile
		if (isNetworkAvailable() == false) {
			Toast.makeText(getActivity(),
					"Please try again when network is available",
					Toast.LENGTH_LONG).show();
			return view;
		}

		signinButton = (Button) view.findViewById(R.id.login_button);
		signinButton.setOnClickListener(this);
		return view;
	}

	@Override
	public void onClick(View v) {
		if (v == null) {
			Toast.makeText(getActivity(),
					"View is null for some reason! Contact the developer",
					Toast.LENGTH_SHORT).show();
			return;
		}

		String deviceId = Secure.getString(getActivity()
				.getApplicationContext().getContentResolver(),
				Secure.ANDROID_ID);
		String email = ((EditText) v.getRootView().findViewById(
				R.id.email_field)).getText().toString();
		String password = ((EditText) v.getRootView().findViewById(
				R.id.password_field)).getText().toString();
		if (email.length() == 0 || password.length() == 0) {
			Toast.makeText(getActivity(), "Please enter email and password",
					Toast.LENGTH_SHORT).show();
			return;
		}

		// Sending to an AsyncTask to POST data
		HttpPost httpPost = new HttpPost("http://erp.saarang.org/mobile/login/");

		List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
		// nameValuePairs.add(new BasicNameValuePair("csrfmiddlewaretoken",
		// csrfToken));
		// nameValuePairs.add(new BasicNameValuePair("name", name));
		nameValuePairs.add(new BasicNameValuePair("email", email));
		// nameValuePairs.add(new BasicNameValuePair("gender", gender));
		// nameValuePairs.add(new BasicNameValuePair("mobile", mobile));
		// nameValuePairs.add(new BasicNameValuePair("college", college));
		nameValuePairs.add(new BasicNameValuePair("password", password));
		// nameValuePairs.add(new BasicNameValuePair("X-CSRFToken", csrfToken));
		nameValuePairs.add(new BasicNameValuePair("key", deviceId));
		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (UnsupportedEncodingException e) {
			// Auto-generated catch block
			e.printStackTrace();
		}
		progressBar.setVisibility(View.VISIBLE);
		new SendPost().execute(httpPost);
	}

	public void renderResponse(String responseContent) {
		String messageToShow;
		if (responseContent == null) {
			messageToShow = "Unknown error, consider filing a bug.";
		} else {
			// Display dialogues for each response
			// char success =
			// responseContent.charAt(responseContent.indexOf("<body>")+7);
			char success = responseContent.charAt(0);
			Log.v("responseChar", String.valueOf(success));

			switch (success) {
			case 'S':
				messageToShow = "Successfully logged in!";
				saarangId = responseContent;
				break;
			case 'W':
				messageToShow = "Wrong password, please try again.";
				break;
			case 'N':
				messageToShow = "Email not registered, please register first.";
				break;
			case 'A':
				messageToShow = "Account has not been activated; try checking your e-mail's spam/junk folder for an e-mail from Saarang.";
				break;
			case 'E':
				messageToShow = "Critical error, please try again later.";
				break;
			default:
				messageToShow = "Unknown response. Please consider filing a bug.";
			}
		}
		AlertDialog.Builder alertBuilder = new AlertDialog.Builder(
				getActivity());
		alertBuilder.setMessage(messageToShow);
		AlertDialog alert = alertBuilder.create();
		alert.show();

		// Send intent to WebView to show the response
		//Intent intent = new Intent(this.getActivity(), ResponseActivity.class);
		//intent.putExtra("response", responseContent);
		//startActivity(intent);
	}

	/**
	 * Executes a HttpPost and calls function with result
	 * 
	 * @author killjoy
	 * 
	 */
	public class SendPost extends AsyncTask<HttpPost, Integer, String> {
		// ProgressDialog progressDialog;

		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			// progressDialog = ProgressDialog.show(,"Please wait...",
			// "Retrieving data ...", true);
		}

		@Override
		protected String doInBackground(HttpPost... params) {

			HttpResponse response = null;
			String responseContent = null;
			try {

				HttpClient httpClient = new DefaultHttpClient();
				// params[0].setHeader("Referer", "saarang.org");
				// params[0].setHeader("X-CSRFToken", csrfToken);

				// final BasicCookieStore cookieStore = new BasicCookieStore();

				// BasicClientCookie csrf_cookie = new
				// BasicClientCookie("csrftoken", csrfToken);
				// csrf_cookie.setDomain(CSRF_COOKIE_DOMAIN);
				// cookieStore.addCookie(csrf_cookie);

				// Create local HTTP context - to store cookies
				// HttpContext localContext = new BasicHttpContext();
				// Bind custom cookie store to the local context
				// localContext.setAttribute(ClientContext.COOKIE_STORE,
				// cookieStore);

				response = httpClient.execute(params[0]);
				responseContent = EntityUtils.toString(response.getEntity());

				Log.v("Response registration", responseContent);
			} catch (ClientProtocolException e) {
				// process exception
			} catch (IOException e) {
				// process exception
			} catch (ParseException e) {
				// Auto-generated catch block
				e.printStackTrace();
			}
			return responseContent;
		}

		protected void onPostExecute(String responseContent) {
			super.onPostExecute(responseContent);
			progressBar.setVisibility(View.INVISIBLE);
			renderResponse(responseContent);
		}

		protected void onProgressUpdate(Integer... progress) {
			// progressBar.setProgress(progress[0]);
		}
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getActivity()
				.getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null && activeNetworkInfo.isConnected();
	}
}