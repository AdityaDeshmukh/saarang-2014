package com.example.registertest;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends FragmentActivity {

	FragmentManager fragmentManager = getSupportFragmentManager();
	Button loginFragment;
	Button registerFragment;
	FragmentTransaction transaction;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		loginFragment = (Button) findViewById(R.id.login_fragment_button);
		registerFragment = (Button) findViewById(R.id.register_fragment_button);
		registerFragment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				fragmentManager.popBackStack();
				transaction = fragmentManager.beginTransaction();
				transaction.add(R.id.container, new RegisterFragment(),
						"Register Fragment");
				transaction.addToBackStack("back");
				transaction.commit();
			}
			
		});
		loginFragment.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				fragmentManager.popBackStack();
				transaction = fragmentManager.beginTransaction();
				transaction.add(R.id.container, new LoginFragment(),
						"Login Fragment");
				transaction.addToBackStack("back");
				transaction.commit();
			}
			
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
}
