package com.example.saaranglivewallpaper;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.service.wallpaper.WallpaperService;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;

public class MyWallpaperService extends WallpaperService {

	@Override
	public Engine onCreateEngine() {
		return new MyWallpaperEngine();
	}

	private class MyWallpaperEngine extends Engine {
		private final Handler handler = new Handler();
		private final Runnable drawRunner = new Runnable() {
			@Override
			public void run() {
				draw();
			}

		};
		// private List<MyPoint> circles;
		// private Paint paint = new Paint();
		private int width;
		int height;
		private boolean visible = true;
		// private int maxNumber;
		// private boolean touchEnabled;
		private boolean offsetChangedWorking = false;
		float dx;
		Bitmap bg, fg;
		int homescreencount=5;
		Context context;

		public MyWallpaperEngine() {
			// SharedPreferences prefs = PreferenceManager
			// .getDefaultSharedPreferences(MyWallpaperService.this);
			// maxNumber = Integer
			// .valueOf(prefs.getString("numberOfCircles", "4"));
			// touchEnabled = prefs.getBoolean("touch", false);
			// circles = new ArrayList<MyPoint>();
			// paint.setAntiAlias(true);
			// paint.setColor(Color.WHITE);
			// paint.setStyle(Paint.Style.STROKE);
			// paint.setStrokeJoin(Paint.Join.ROUND);
			// paint.setStrokeWidth(10f);
			handler.post(drawRunner);
			context = getApplicationContext();
		}

		@Override
		public void onVisibilityChanged(boolean visible) {
			this.visible = visible;
			if (visible) {
				handler.post(drawRunner);
			} else {
				handler.removeCallbacks(drawRunner);
			}
		}

		@Override
		public void onSurfaceDestroyed(SurfaceHolder holder) {
			super.onSurfaceDestroyed(holder);
			this.visible = false;
			handler.removeCallbacks(drawRunner);
		}

		@Override
		public void onSurfaceChanged(SurfaceHolder holder, int format,
				int width, int height) {
			this.width = width;
			this.height = height;
			setbgfg(R.drawable.wallpaper2,R.drawable.saw_teeth);
			super.onSurfaceChanged(holder, format, width, height);
		}

		// // TODO Find out why the error is occurring.
		// @Override
		// public void onResume() {
		// this.offsetChangedWorking = false;
		// }

		// @Override
		// public void onTouchEvent(MotionEvent event) {
		//
		// if (offsetChangedWorking == false) {
		// // TODO implement if OnOffesetsChanged isn't working
		// }
		// if (touchEnabled) {
		// float x = event.getX();
		// float y = event.getY();
		// SurfaceHolder holder = getSurfaceHolder();
		// Canvas canvas = null;
		// try {
		// canvas = holder.lockCanvas();
		// if (canvas != null) {
		// canvas.drawColor(Color.BLACK);
		// circles.clear();
		// circles.add(new MyPoint(
		// String.valueOf(circles.size() + 1), (int) x,
		// (int) y));
		// drawCircles(canvas, circles);
		//
		// }
		// } finally {
		// if (canvas != null)
		// holder.unlockCanvasAndPost(canvas);
		// }
		// super.onTouchEvent(event);
		// }
		// }
		//
		private void draw() {
			SurfaceHolder holder = getSurfaceHolder();
			Canvas canvas = null;
			try {
				canvas = holder.lockCanvas();
				if (canvas != null) {
					// Parallax images
					canvas.save();
					canvas.drawBitmap(bg, 0, 0, null);
					canvas.drawBitmap(fg, 0.0f, 700f, null);
					canvas.restore();
					Log.v("in draw", "in draw");
				}
			} finally {
				if (canvas != null)
					holder.unlockCanvasAndPost(canvas);
			}
			//handler.removeCallbacks(drawRunner);
			if (visible) {
				//handler.postDelayed(drawRunner, 5000);
			}
		}

		//
		// // Surface view requires that all elements are drawn completely
		// private void drawCircles(Canvas canvas, List<MyPoint> circles) {
		// canvas.drawColor(Color.BLACK);
		// for (MyPoint point : circles) {
		// canvas.drawCircle(point.x, point.y, 20.0f, paint);
		// }
		// }

		public void setbgfg(int imageId,int imageId2) {
			//Log.v("(homescrncount-1)/10", Integer.toString(1 + (homescreencount-1)/10));
			bg = BitmapFactory.decodeResource(context.getResources(), imageId);
			bg = Bitmap.createScaledBitmap(bg, (int) (width * (1 + (float)(homescreencount-1)/10)), height, true);

			fg = BitmapFactory.decodeResource(context.getResources(), imageId2);
			fg = Bitmap.createScaledBitmap(fg, (int) (width * (1 + (float)(homescreencount-1)/10*2)), height/2, true);
			

			//Log.v("width", Integer.toString(width));
			//Log.v("height", Integer.toString(height));
			//Log.v("bgwidth", Integer.toString(bg.getWidth()));
		}

		/*
		 * Parallax effect with the images, called everytime the screen moves.
		 */
		@Override
		public void onOffsetsChanged(float xOffset, float yOffset,
				float xOffsetStep, float yOffsetStep, int xPixelOffset,
				int yPixelOffset) {
			if (offsetChangedWorking == false && xOffset != 0.0f
					&& xOffset != 0.5f) {
				offsetChangedWorking = true;
			}

			if (offsetChangedWorking == true) {
				homescreencount =(int) (1/xOffsetStep+1);
				//Log.v("homescreenscount",Integer.toString(homescreencount));
				//dx = (width - bg.getWidth()) * (1-xOffset);
				dx = (width - bg.getWidth()) * (1-xOffset);
				//Log.v("dx", Float.toString(dx));
				SurfaceHolder holder = getSurfaceHolder();
				Canvas canvas = null;
				//Log.v("in onOffsetsChanges", "in onOffsetsChanged");
				try {
					canvas = holder.lockCanvas();
					if (canvas != null) {
						// Parallax images
						canvas.save();
						canvas.translate(dx, 0);
						canvas.drawBitmap(bg, 0f, 0f, null);
						canvas.translate(dx, 0);
						canvas.drawBitmap(fg, 0.0f, 700f, null);
						canvas.restore();
					}
				} finally {
					if (canvas != null)
						holder.unlockCanvasAndPost(canvas);
					//super.onOffsetsChanged(xOffset, yOffset, xOffsetStep,yOffsetStep, xPixelOffset, yPixelOffset);
				}
			}
		}
	}
}