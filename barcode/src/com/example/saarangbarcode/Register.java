package com.example.saarangbarcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

public class Register extends Activity implements OnClickListener {
	String scanContent, scanFormat, result, resPage, discount, user, pass;
	String[][] data = new String[3][];
	Spinner spinner;
	Button b, bEntry, sendTeam;
	TextView formatTxt, contentTxt, tv1, tvr1, tvr2;
	EditText et, etAdd;
	int pos;
	long l;
	String[] myStrings;
	boolean scanDone = false, httpRes = false, Is_Team;
	Database db;
	EventDatabase eveDb;
	StringBuilder team = new StringBuilder();
	public ProgressDialog mDialog;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		pos=0;
		l = getIntent().getLongExtra("position", 0);
		setContentView(R.layout.register);
		et = (EditText) findViewById(R.id.etEntry);
		bEntry = (Button) findViewById(R.id.bEntry);
		sendTeam = (Button) findViewById(R.id.sendTeam);
		bEntry.setOnClickListener(this);
		bEntry.setText("Add Entry");
		user = getIntent().getStringExtra("user");
		pass = getIntent().getStringExtra("pass");
		Log.i("regUser", user);
		Log.i("regPass", pass);
		spinner = (Spinner) findViewById(R.id.s1);
		b = (Button) findViewById(R.id.button1);
		formatTxt = (TextView) findViewById(R.id.tvFor);
		contentTxt = (TextView) findViewById(R.id.tvCon);
		tv1 = (TextView) findViewById(R.id.textView01);
		tvr1 = (TextView) findViewById(R.id.tvr1);
		tvr2 = (TextView) findViewById(R.id.tvr2);
		db = new Database(this);
		b.setOnClickListener(this);
		sendTeam.setOnClickListener(this);
		eveDb = new EventDatabase(this);
		eveDb.open();
		data = eveDb.getData();
		List<String> categories = new ArrayList<String>();
		for (int i = 0; i < data[0].length; i++) {
			categories.add(data[1][i]);
		}
		eveDb.close();

		ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this,
				android.R.layout.simple_spinner_item, categories);
		dataAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinner.setAdapter(dataAdapter);
		spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				Log.e("OnItemSelected", ">>"+arg2);
				if(pos!=arg2){
					team.setLength(0);
				}
				pos = arg2;
				Is_Team = data[2][arg2].equals("1");
				//Log.e("Team", ""+data[2][arg2]+" "+is_team);
				if(Is_Team)
					sendTeam.setVisibility(View.VISIBLE);
					
				else
					sendTeam.setVisibility(View.GONE);
			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				Log.e("OnNothingSelected", ">> duh");
			}
		});
	}

	@SuppressLint("NewApi")
	@Override
	public void onClick(View v) {		if (v.getId() == R.id.button1) {
			IntentIntegrator scanIntegrator = new IntentIntegrator(this);
			// start scanning
			try {
				scanIntegrator.initiateScan();
			} catch (Exception e) {
				Log.e("<---Scan Error 1--->", ">>" + e.getMessage());
			}
			tvr1.setText("Turning Camera On...");
			tvr2.setText("");
		} else if (v.getId() == R.id.bEntry) {
			// Log.i("hjh", "hsdgchjsa");
			scanContent = et.getText().toString();
			if(Is_Team){
				tvr1.setText("Add members and then press 'Send Team!'");
				formatTxt.setText("FORMAT: " + scanFormat);
				contentTxt.setText("CONTENT: " + scanContent);
				tvr2.setText("");
				if(scanContent!=null&&!scanContent.isEmpty()){
					team.append(scanContent);
					team.append("/");
				}else{
					tvr1.setText("Empty Field");
					Log.e("<--Emplty Field-->", "empty edit text");
				}
			}else{
			if(scanContent!=null&&!scanContent.isEmpty()){
				Log.e("test", ">>"+scanContent);
				scanDone = true;
				scanFormat = "1";
				tvr1.setText("Sending...");
				sendData();
			} else {
				tvr1.setText("Empty Field");
				Log.e("<--Emplty Field-->", "empty edit text");
			}
			}
		} else if (v.getId() == R.id.sendTeam){
			Log.e("SEND BUTTON","PRESSED");
			scanContent = team.toString();
			Log.e("TEAM : ", scanContent);
			scanDone = true;
			scanFormat = "2";
			if(scanContent!=null && !scanContent.isEmpty())
				sendData();
			else{
				tvr1.setText("Empty Field");
				tvr2.setText("");
				Log.e("<--Emplty Field-->", "empty edit text");
			}
			team.setLength(0);
		}
	}

	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		// retrieve result of scanning - instantiate ZXing object
		IntentResult scanningResult = IntentIntegrator.parseActivityResult(
				requestCode, resultCode, intent);

		// Check for resultCode showing un-executed activity
		if (resultCode == RESULT_CANCELED) {
			Log.v("debug back crash", resultCode + "");
			// invalid scan data or scan canceled
			Toast toast = Toast.makeText(getApplicationContext(),
					"No scan data received!", Toast.LENGTH_SHORT);
			toast.show();
			return;
		}

		// check we have a valid result
		if (scanningResult != null) {
			if(Is_Team){
				tvr1.setText("Add members and then press 'Send Team!'");
				scanContent = scanningResult.getContents();
				scanFormat = scanningResult.getFormatName();
				formatTxt.setText("FORMAT: " + scanFormat);
				contentTxt.setText("CONTENT: " + scanContent);
				Log.i("Inside onActivityResult", "reached");
				//TODO verify
				if(scanContent!=null/*&&!scanContent.isEmpty()*/){
					team.append(scanContent);
					team.append("/");
				}
			}
			else{
			tvr1.setText("Sending...");
			// get content from Intent Result
			scanContent = scanningResult.getContents();
			// get format name of data scanned
			scanFormat = scanningResult.getFormatName();
			// output to UI
			formatTxt.setText("FORMAT: " + scanFormat);
			contentTxt.setText("CONTENT: " + scanContent);
			Log.i("Inside onActivityResult", "reached");
			scanDone = true;
			if (scanContent != null) {
				sendData();
			} else {
				tvr1.setText("No data scanned");
				Log.e("<---Error Scan--->", ">>No Data Scanned");
			}
			}
		} else {
			// invalid scan data or scan canceled
			Toast toast = Toast.makeText(getApplicationContext(),
					"No scan data received!", Toast.LENGTH_SHORT);
			toast.show();
		}
	}

	public void postData() throws InterruptedException {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost(
				"http://erp.saarang.org/mobile/barcode/");
		while (true) {
			if (scanDone == true && scanFormat != null && scanContent != null) {
				try {
					Log.i("Inside postData", "reached");
					List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(
							2);
					nameValuePairs.add(new BasicNameValuePair("user", user));
					nameValuePairs.add(new BasicNameValuePair("pass", pass));
					nameValuePairs.add(new BasicNameValuePair("userid",
							scanContent));
					nameValuePairs.add(new BasicNameValuePair("actionType",
							String.valueOf(l)));
					nameValuePairs.add(new BasicNameValuePair("eventid",
							data[0][pos]));
					httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
					// Execute HTTP Post Request
					HttpResponse response = httpclient.execute(httppost);
					HttpEntity entity = response.getEntity();
					InputStream is = entity.getContent();
					result = convertStreamToString(is);
					httpRes = true;
					Log.i("RESULT", result);
					resPage = response.getStatusLine().getReasonPhrase();
					Log.i("httpsRes true", "reached");
					Log.i("Page", response.getStatusLine().getReasonPhrase());
				} catch (ClientProtocolException e) {
					Log.e("<---Client Protocol Exception--->", e.getMessage());
				} catch (IOException e) {
					Log.e("<---IO Exception--->", e.getMessage());
				} catch (Exception e) {
					Log.e("<---Inside PostData Other Error--->", e.getMessage());
				}
				break;
			} else {
				Thread.sleep(10);
			}
		}
	}

	private String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}

	private void sendData() {
		Thread networkThread = new Thread(new Runnable() {
			@Override
			public void run() {
				// Log.i("hjh", "hsdgchjsa");
				try {
					postData();
					if (l == 1 && httpRes && resPage.equals("OK")
							&& result.charAt(0) == 'S') {
						db.open();
						db.createEntry(scanContent, data[1][pos], data[0][pos],
								1);
						Log.i("SEND", "DONE");
						db.close();
					} else if (l == 1 && (!httpRes || !resPage.equals("OK"))) {
						db.open();
						boolean found = db.findEntry(scanContent, data[0][pos]);
						Log.i("FOUND", "" + found);
						if (!found) {
							db.createEntry(scanContent, data[1][pos],
									data[0][pos], 0);
							Log.i("STORE", "DONE");
						}
						db.close();
					}
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (Exception e) {
					Log.e("Test", e.getMessage());
				}
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						tvr1.setText("Stored internally");
						tvr2.setText("Sending Unsuccesful!");
					}
				});
			}
		});
		networkThread.start();
	}
}
