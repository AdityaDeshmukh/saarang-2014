package com.example.saarangbarcode;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.annotation.SuppressLint;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

@SuppressLint("HandlerLeak")
public class Choose extends ListActivity {
	int pos, count = 0;
	Thread networkThread;
	Boolean httpRes = false;
	String result, resPage, user, pass;
	EventDatabase db;
	Database info;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		user = getIntent().getStringExtra("user");
		pass = getIntent().getStringExtra("pass");
		Log.i("chooseUser", user);
		Log.i("choosePass", pass);
		
		String[] choices = { "Scan and Register", "Get list from server","Refresh Event List", "Show scanned data","Export to csv" };
		setListAdapter(new ArrayAdapter<String>(Choose.this, android.R.layout.simple_list_item_1, choices));
		setContentView(R.layout.choose);
	}

	public ProgressDialog mDialog;
	private Handler handler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			if (msg.what == 0)
				mDialog.dismiss();
			else if (msg.what == 1)
				mDialog = ProgressDialog.show(Choose.this, "Fetching Data",
						"Loading Please Wait", true);
			else if (msg.what == 2)
				mDialog = ProgressDialog.show(Choose.this, "Connection Error",
						"Cannot connect to server", true);
			else if (msg.what == 3)
				mDialog = ProgressDialog.show(Choose.this,
						"Refreshing Event List", "Loading Please Wait", true);
		}
	};

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		// TODO Auto-generated method stub
		super.onListItemClick(l, v, position, id);
		pos = position + 1;
		db = new EventDatabase(this);
		info = new Database(this);
		networkThread = new Thread(new Runnable() {
			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					switch(pos){
					case 1 :
						Intent reg = new Intent(Choose.this, Register.class);
						reg.putExtra("position", (long) (pos));
						reg.putExtra("user", user);
						reg.putExtra("pass", pass);
						startActivity(reg);
						break;
					case 2 :
						Intent list = new Intent(Choose.this, GetList.class);
						list.putExtra("user", user);
						list.putExtra("pass", pass);
						startActivity(list);
						break;
					case 3 :
						handler.sendEmptyMessage(3);
						postData();
						if (httpRes) {
							String[] myStrings = result.split(":");
							db.open();
							db.update();
							for (int i = 1; i < myStrings.length; i++) {
								//Log.e(""+i, ""+myStrings[i]);
								if (i % 3 == 0)
									db.createEntry(myStrings[i-1], myStrings[i - 2], myStrings[i]);
							}
							db.close();
							handler.sendEmptyMessage(0);
						} else {
							handler.sendEmptyMessage(0);
							handler.sendEmptyMessage(2);
							Thread.sleep(2500);
							handler.sendEmptyMessage(0);
						}
						break;
					case 4 :
						Intent table = new Intent(Choose.this, Display.class);
						table.putExtra("user", user);
						table.putExtra("pass", pass);
						startActivity(table);
						break;
					case 5 :
						info.open();
						try {
							info.dbToCsv();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						info.close();
						break;
					}
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		networkThread.start();
	}

	public void postData() throws InterruptedException {
		// Create a new HttpClient and Post Header
		HttpClient httpclient = new DefaultHttpClient();
		HttpPost httppost = new HttpPost("http://erp.saarang.org/mobile/barcode/");
		try {
			Log.i("Inside postData", "reached");
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
			nameValuePairs.add(new BasicNameValuePair("user", user));
			nameValuePairs.add(new BasicNameValuePair("pass", pass));
			nameValuePairs.add(new BasicNameValuePair("actionType", String.valueOf(4)));
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			// Execute HTTP Post Request
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			InputStream is = entity.getContent();
			result = convertStreamToString(is);
			httpRes = true;
			resPage = response.getStatusLine().getReasonPhrase();
			Log.i("httpsRes true", "reached");
			Log.i("RESULT", result);
			Log.i("Page", response.getStatusLine().getReasonPhrase());
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}
	}

	private String convertStreamToString(InputStream is) {

		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder sb = new StringBuilder();
		String line = null;
		try {
			while ((line = reader.readLine()) != null) {
				sb.append((line + "\n"));
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return sb.toString();
	}
}
