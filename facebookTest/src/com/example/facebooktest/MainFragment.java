package com.example.facebooktest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import oauth.signpost.OAuthConsumer;
import oauth.signpost.commonshttp.CommonsHttpOAuthConsumer;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;

public class MainFragment extends Fragment implements OnClickListener{

	private static final String TAG = "MainFragment";
	private UiLifecycleHelper uiHelper;
	private TextView userInfoTextView;
	protected HttpPost httppost;
	private String SEND_DATA_URL = "http://www.saarang.iitm.ac.in/main_backend/register.php";
	private String facebookAppKey="419418218183847";
	private String facebookAppSecret="5f5dd285e353765c3784f950192ff498";

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.main, container, false);

		userInfoTextView = (TextView) view.findViewById(R.id.userInfoTextView);

		LoginButton authButton = (LoginButton) view
				.findViewById(R.id.authButton);
		authButton.setFragment(this);
		authButton.setReadPermissions(Arrays.asList("email"));

        Button b = (Button) view.findViewById(R.id.registerButton);
        b.setOnClickListener(this);
        b=(Button)view.findViewById(R.id.shareButton);
        b.setOnClickListener(this);
		return view;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(getActivity(), callback);
		uiHelper.onCreate(savedInstanceState);
	}

	@Override
	public void onResume() {
		super.onResume();

		// For scenarios where the main activity is launched and user
		// session is not null, the session state change notification
		// may not be triggered. Trigger it if it's open/closed.
		Session session = Session.getActiveSession();
		if (session != null && (session.isOpened() || session.isClosed())) {
			onSessionStateChange(session, session.getState(), null);
		}

		uiHelper.onResume();
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		uiHelper.onActivityResult(requestCode, resultCode, data);
	}

	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
	}

	private Session.StatusCallback callback = new Session.StatusCallback() {
		@Override
		public void call(Session session, SessionState state,
				Exception exception) {
			onSessionStateChange(session, state, exception);
		}
	};

	private void onSessionStateChange(Session session, SessionState state,
			Exception exception) {
		if (state.isOpened()) {
			Log.i(TAG, "Logged in...");

			userInfoTextView.setVisibility(View.VISIBLE);

			// Request user data and show the results
			Request.newMeRequest(session, new Request.GraphUserCallback() {

				@Override
				public void onCompleted(GraphUser user, Response response) {
					if (user != null) {
						// Display the parsed user info
						userInfoTextView.setText(buildUserInfoDisplay(user));
					}
				}
			}).executeAsync();
//			
//			// Send data to server to register
//        	Log.v(TAG, "Registering");
//    		new SendDataRegisterTask()
//    				.execute(SEND_DATA_URL);			
			
			Log.i(TAG, "Got data...");
		} else if (state.isClosed()) {
			userInfoTextView.setVisibility(View.INVISIBLE);
			Log.i(TAG, "Logged out...");
		}
	}

	private String buildUserInfoDisplay(GraphUser user) {
		StringBuilder userInfo = new StringBuilder("");

		// Logging HTTP data
		Settings.addLoggingBehavior(LoggingBehavior.REQUESTS);

		// Example: typed access (name)
		// - no special permissions required
		userInfo.append(String.format("Name: %s\n\n", user.getName()));

		// Example: typed access (birthday)
		// - requires user_birthday permission
		userInfo.append(String.format("Birthday: %s\n\n", user.getBirthday()));

		// Example: partially typed access, to location field,
		// name key (location)
		// - requires user_location permission
		userInfo.append(String.format("Location: %s\n\n", user.getLocation()
				.getProperty("name")));

		// Example: access via property name (locale)
		// - no special permissions required
		userInfo.append(String.format("Locale: %s\n\n",
				user.getProperty("locale")));

		// Example: access via key for array (languages)
		// - requires user_likes permission
		JSONArray languages = (JSONArray) user.getProperty("languages");
		if (languages.length() > 0) {
			ArrayList<String> languageNames = new ArrayList<String>();
			for (int i = 0; i < languages.length(); i++) {
				JSONObject language = languages.optJSONObject(i);
				// Add the language name to a list. Use JSON
				// methods to get access to the name field.
				languageNames.add(language.optString("name"));
			}
			userInfo.append(String.format("Languages: %s\n\n",
					languageNames.toString()));
		}

		// Getting email
		userInfo.append(String.format("Email:  %s\n\n",
				user.getProperty("email")));

		// Getting gender
		userInfo.append(String.format("Gender: %s\n\n",
				user.getProperty("gender")));

		// Getting user_id
		userInfo.append(String.format("User ID: %s\n\n", user.getId()));

		return userInfo.toString();
	}

	public HttpPost getDataRegister(GraphUser user, Session session,
			String urlString) {
		HttpPost httppost = new HttpPost(urlString);

		try {
			// Add the data
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(7);
			String token = session.getAccessToken();
			nameValuePairs.add(new BasicNameValuePair("oauth_token", token));
			nameValuePairs.add(new BasicNameValuePair("user_id", user.getId()));
			nameValuePairs.add(new BasicNameValuePair("name", user.getName()));
			nameValuePairs.add(new BasicNameValuePair("email", user
					.getProperty("email").toString()));
			nameValuePairs.add(new BasicNameValuePair("gender", user
					.getProperty("gender").toString()));
			nameValuePairs.add(new BasicNameValuePair("mobile", getActivity()
					.findViewById(R.id.phoneNo).toString()));
			nameValuePairs.add(new BasicNameValuePair("password", getActivity()
					.findViewById(R.id.password).toString()));
			
			//Facebook signing
			 // create a consumer object and configure it with the access
			   // token and token secret obtained from the service provider
			    OAuthConsumer consumer = new CommonsHttpOAuthConsumer(facebookAppKey, facebookAppSecret);
			    consumer.setTokenWithSecret(token, facebookAppSecret);

			   // sign the request      
			    consumer.sign(httppost);       
			
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
		} catch (IOException e) {
			// TODO Auto-generated catch block
		}

		catch (Exception ex) {
			Log.e("Debug", "error: " + ex.getMessage(), ex);
		}
		return httppost;
	}
	
	@Override
    public void onClick(View v) {
		Log.v(TAG, "in OnClick");
        switch (v.getId()) {
        case R.id.registerButton:
        	// Send data to server to register
        	Log.v(TAG, "Registering");
    		new SendDataRegisterTask()
    				.execute(SEND_DATA_URL);
    		
    		break;
        case R.id.shareButton:
        	Log.v(TAG, "Sharing");
    		FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(
    				this.getActivity()).setLink("http://www.saarang.org/").build();
    		uiHelper.trackPendingDialogCall(shareDialog.present());
    		
    		break;
        }
    }
	
	class SendDataRegisterTask extends AsyncTask<String, Void, String> {

		private Exception exception;
		protected String doInBackground(String... url) {
			Log.v(TAG, "in SendDataRegisterTask");
			final String urlString = url[0];
			// Create a new HttpClient and Post Header
			HttpClient httpclient = new DefaultHttpClient();
			httppost = new HttpPost(urlString);
			try {
				Session session = Session.getActiveSession();
				Request.newMeRequest(session, new Request.GraphUserCallback() {
					@Override
					public void onCompleted(GraphUser user, Response response) {
						if (user != null) {

							// Get user data
							Session session2 = Session.getActiveSession();
							// Execute HTTP Post Request
							httppost = getDataRegister(user, session2, urlString);
							Log.v(TAG, "Got httppost");
						}
					}
				}).executeAsync();
			} catch (Exception e) {
				this.exception = e;
			}
			HttpResponse response2 = null;
			try {
				response2 = httpclient.execute(httppost);
				Log.v(TAG, "Got HttpResponse");
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(response2!=null)Log.v("The response is", response2.toString());
			else Log.v("The response is", "null");
			return response2.toString();
		}

		protected void onPostExecute() {
			// TODO: check this.exception
			// TODO: do something
		}
	}
}
